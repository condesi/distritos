<?php

//RADIO BUTTONS 
//add_action( 'woocommerce_review_order_before_payment', 'wp_checkout_radio_choice' ); 
add_action( 'woocommerce_review_order_before_payment', 'wp_checkout_radio_choice'); 
function wp_checkout_radio_choice() {
      echo '<div id="container-data-radio">
    </div>';    
    
}

add_action( 'woocommerce_cart_calculate_fees', 'wp_checkout_radio_choice_fee', 20, 1 );
function wp_checkout_radio_choice_fee( $cart ) {
   if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;
   $radio = WC()->session->get( 'radio_chosen' ); 
   if ( $radio ) 
   {
      $texto = explode('-',$radio);
      if($texto[1] == 'recojo')
      {
        $texto[1] = 'Recojo en Almacén';
      }
      $texto_envio = 'Envio '.$texto[1];
      $valor_fee = $texto[0];
      $cart->add_fee($texto_envio,$valor_fee);
   }
}
add_action( 'woocommerce_checkout_update_order_review', 'wp_checkout_radio_choice_set_session' ); 
function wp_checkout_radio_choice_set_session( $posted_data ) {
    parse_str( $posted_data, $output );
    if ( isset( $output['radio_choice'] ) ){
        WC()->session->set( 'radio_chosen', $output['radio_choice'] );
    }
}



//=======================================================================================
add_filter('woocommerce_checkout_fields', 'default_values_checkout_fields');
function default_values_checkout_fields($fields) {    
    
      unset($fields['billing']['billing_postcode']);
      unset($fields['shipping']['shipping_postcode']);
     
      return $fields;
}



function tofloat($num) {
    $dotPos = strrpos($num, '.');
    $commaPos = strrpos($num, ',');
    $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos : 
        ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);
    if (!$sep) {
        return floatval(preg_replace("/[^0-9]/", "", $num));
    } 
    return floatval(
        preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
        preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
    );
}


add_action('wp_ajax_woocommerce_apply_district', 'calculate', 10);
add_action('wp_ajax_nopriv_woocommerce_apply_district', 'calculate', 10);
function calculate() {
      $exist_distrito = 0;
      $distrito_break = '';
      $distrito_break_opcion = '';
      $express_break_opcion = '';
      $programado_break_opcion = '';

      $express_break = '';
      $programado_break = '';

   

      $express = 0;
      $programado = 0;
      $mygateways = array();
      if (isset($_POST['region'])) {
          global $woocommerce;
          $district = $_POST['district'];
          if ($_POST['region']!=="") {
             
             

              $recoveredData = file_get_contents(wooshc_path.'includes/wooshc_option.txt');
              
              $wooshc_option = unserialize($recoveredData);
              if(count($wooshc_option) == 0)
              {
                $wooshc_option = get_option('wooshc_option',true);
              }

              
              foreach ($wooshc_option as $key => $value) {
        
                
                  if($wooshc_option[$key]['regiones'] == $_POST['region'])
                  {
                       $val = $wooshc_option[$key]['precios'];
                       $express = $wooshc_option[$key]['express'];
                       $programado = $wooshc_option[$key]['programado'];
                       $exist_distrito = 1;
                       if(intval($_POST['province']) == 0 && intval($wooshc_option[$key]['provincias'])==0){
                         $distrito_break = $wooshc_option[$key]['precios'];
                         $express_break = $wooshc_option[$key]['express'];
                         $programado_break = $wooshc_option[$key]['programado'];
                      }
                  }
               
                if(intval($wooshc_option[$key]['regiones']) > 0  && intval($wooshc_option[$key]['provincias']) >0  &&  intval($_POST['province'])>0 && intval($_POST['region'])>0)
                {
                  if($wooshc_option[$key]['regiones'] == $_POST['region'] &&  $wooshc_option[$key]['provincias']==$_POST['province'])
                  {
                     $val = $wooshc_option[$key]['precios'];
                     $express = $wooshc_option[$key]['express'];
                     $programado = $wooshc_option[$key]['programado'];

                     $exist_distrito = 1;
                  }else{

                     foreach ($wooshc_option as $key_repe1 => $value_repe1) {
                         if($wooshc_option[$key]['regiones'] == $_POST['region'])
                        {
                             $val = $wooshc_option[$key_repe1]['precios'];
                             $express = $wooshc_option[$key_repe1]['express'];
                             $programado = $wooshc_option[$key_repe1]['programado'];
                             $exist_distrito = 1;
                             break;
                        }
                     }
                  }
                }

                //veremos si no es un arreglo
                if(isset($wooshc_option[$key]['distritos'])){
                  if(!is_array($wooshc_option[$key]['distritos']))
                  {
                   //lo inicializamos
                   $wooshc_option[$key]['distritos'] = array();
                  }
                }else{
                  $wooshc_option[$key]['distritos'] = array();
                }
               
                foreach ($wooshc_option[$key]['distritos'] as $key2 => $value2) 
                { 

                    if( ($wooshc_option[$key]['distritos'][$key2] == $_POST['district']) && ($wooshc_option[$key]['regiones']==$_POST['region']) && ($wooshc_option[$key]['provincias']==$_POST['province']) ) {
                      $val = $wooshc_option[$key]['precios'];
                      $express = $wooshc_option[$key]['express'];
                      $programado = $wooshc_option[$key]['programado'];
                      
                      $exist_distrito = 1;

                      $distrito_break = $wooshc_option[$key]['precios'];
                      $express_break = $wooshc_option[$key]['express'];
                      $programado_break = $wooshc_option[$key]['programado'];

                      break;
                    }
                }

                if($distrito_break != '')
                {
                      $val = $distrito_break;
                      $express = $express_break;
                      $programado = $programado_break;
                }


              }


              
              $amount = tofloat($woocommerce->cart->get_cart_total());
             
              $total_items = 0;
              if ( sizeof( $woocommerce->cart->get_cart() ) > 0 ) {
                  foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values )
                  {
                      $_product = $values['data'];
                      $weight = floatval($_product->weight);
                      $total_items = $total_items + $weight; 
                  }
              }
              
              $total_peso = $total_items;






              /**************************
                        CONDICIONES

              **********************************/

              
              $recoveredData = file_get_contents(wooshc_path.'includes/wooshc_condition.txt');
              
              $wooshc_condition = unserialize($recoveredData);
              if(count($wooshc_condition) == 0)
              {
                $wooshc_condition = get_option('wooshc_condition',true);
              }


              $provincia_valor = 0;
              foreach ($wooshc_condition as $key_condicion => $value) {

                //================================================================================================================

               
                if(count($wooshc_condition[$key_condicion]['distritos'])>=0 && intval($wooshc_condition[$key_condicion]['provincias']) >=0){

                    if($wooshc_condition[$key_condicion]['regiones'] == $_POST['region'])
                    {
                      
                        

                            $compare = (string) $wooshc_condition[$key_condicion]['mayor_menor_costo'];
                            $compare_articulo = (string) $wooshc_condition[$key_condicion]['mayor_menor_peso'];
                            $and_or = (string) $wooshc_condition[$key_condicion]['y_o'];
                          
                            if($compare=='=')
                            {
                              $compare = '==';
                            }
                            if($compare_articulo=='='){
                              $compare_articulo = '==';
                            }
                            //sacamos el rango de pesos
                            $split_peso = explode('-',$wooshc_condition[$key_condicion]["resultado_peso"]);

                            //si peso es distinto de vacio
                            if($split_peso[1]!=''){

                            $code='if($amount '.$compare.' floatval($wooshc_condition[$key_condicion]["resultado_costo"]) '.$and_or.' ($total_peso>=$split_peso[0] && $total_peso<=$split_peso[1]))
                                {
                                   //$val = $wooshc_condition[$key_condicion]["precios"];
                                  //$val = $total_peso;
                                  $exist_distrito = 1;

                                  if(intval($_POST["province"]) == 0 && intval($wooshc_condition[$key_condicion]["provincias"])==0)
                                  {
                                     $distrito_break_opcion = $wooshc_condition[$key_condicion]["precios"];
                                     $express_break_opcion = $wooshc_condition[$key_condi]["express"];
                                     $programado_break_opcion = $wooshc_condition[$key_condi]["programado"];
                                  }

                              };';

                            }else{
                              $code='if($amount '.$compare.' floatval($wooshc_condition[$key_condicion]["resultado_costo"]))
                                {
                                  // $val = $wooshc_condition[$key_condicion]["precios"];
                                  //$val = $total_peso;
                                  $exist_distrito = 1;

                                  if(intval($_POST["province"]) == 0 && intval($wooshc_condition[$key_condicion]["provincias"])==0)
                                  {
                                     $distrito_break_opcion = $wooshc_condition[$key_condicion]["precios"];
                                     $express_break_opcion = $wooshc_condition[$key_condi]["express"];
                                     $programado_break_opcion = $wooshc_condition[$key_condi]["programado"];
                                  }

                              };';
                            }
                            echo eval($code);

                    }

              }


                //}
                //Buscaemos por region % provincia
               if(count($wooshc_condition[$key_condicion]['distritos'])<=0)
               {

                    if(intval($wooshc_condition[$key_condicion]['regiones']) > 0  && intval($wooshc_condition[$key_condicion]['provincias']) >0  &&  intval($_POST['province'])>0 && intval($_POST['region'])>0)
                    {
                      if($wooshc_condition[$key_condicion]['regiones'] == $_POST['region'] &&  $wooshc_condition[$key_condicion]['provincias']==$_POST['province'])
                      {

                            //si posee distrito no va a entrar por aqui
                            if(intval($_POST['district'])<=0 && count($wooshc_condition[$key_condicion]['distritos'])>0)
                            {
                              continue;
                            }


                            $compare = (string) $wooshc_condition[$key_condicion]['mayor_menor_costo'];
                            $compare_articulo = (string) $wooshc_condition[$key_condicion]['mayor_menor_peso'];
                            $and_or = (string) $wooshc_condition[$key_condicion]['y_o'];
                          
                            if($compare=='=')
                            {
                              $compare = '==';
                            }
                            if($compare_articulo=='='){
                              $compare_articulo = '==';
                            }
                            //sacamos el rango de pesos
                            $split_peso = explode('-',$wooshc_condition[$key_condicion]["resultado_peso"]);

                            //si peso es distinto de vacio
                            if($split_peso[1]!=''){

                            $code='if($amount '.$compare.' floatval($wooshc_condition[$key_condicion]["resultado_costo"]) '.$and_or.' ($total_peso>=$split_peso[0] && $total_peso<=$split_peso[1]))
                                {
                                  $val = $wooshc_condition[$key_condicion]["precios"];
                                  $express = $wooshc_condition[$key_condicion]["express"];
                                  $programado = $wooshc_condition[$key_condicion]["programado"];

                                  //$val = $total_peso;
                                  $exist_distrito = 1;

                                  $distrito_break_opcion = $wooshc_condition[$key_condicion]["precios"];
                                  $express_break_opcion = $wooshc_condition[$key_condicion]["express"];
                                  $programado_break_opcion = $wooshc_condition[$key_condicion]["programado"];

                                  if(intval($_POST["district"])<=0){
                                    $provincia_valor = $wooshc_condition[$key_condicion]["precios"];
                                    $express = $wooshc_condition[$key_condicion]["express"];
                                    $programado = $wooshc_condition[$key_condicion]["programado"];
                                  }

                              };';

                            }else{
                              $code='if($amount '.$compare.' floatval($wooshc_condition[$key_condicion]["resultado_costo"]))
                                {
                                  $val = $wooshc_condition[$key_condicion]["precios"];
                                  $express = $wooshc_condition[$key_condicion]["express"];
                                  $programado = $wooshc_condition[$key_condicion]["programado"];

                                  //$val = $total_peso;
                                  $exist_distrito = 1;
                                  $distrito_break_opcion = $wooshc_condition[$key_condicion]["precios"];
                                  if(intval($_POST["district"])<=0){
                                      $provincia_valor = $wooshc_condition[$key_condicion]["precios"];
                                  }
                              };';
                            }
                         
                            echo eval($code);

                      }else{

                    
                      }//repetiremos el foreach a ver si contiene la region para darsela al de la regon
                    }

              }

                //================================================================================================================



                foreach ($wooshc_condition[$key_condicion]['distritos'] as $key_condicion2 => $value2) { 
                 
                   if($wooshc_condition[$key_condicion]['distritos'][$key_condicion2] == $_POST['district'])
                   {

                            $compare = (string) $wooshc_condition[$key_condicion]['mayor_menor_costo'];
                            $compare_articulo = (string) $wooshc_condition[$key_condicion]['mayor_menor_peso'];
                            $and_or = (string) $wooshc_condition[$key_condicion]['y_o'];
                          
                            if($compare=='=')
                            {
                              $compare = '==';
                            }
                            if($compare_articulo=='='){
                              $compare_articulo = '==';
                            }
                            //sacamos el rango de pesos
                            $split_peso = explode('-',$wooshc_condition[$key_condicion]["resultado_peso"]);

                            //si peso es distinto de vacio
                            if($split_peso[1]!=''){

                            $code='if($amount '.$compare.' floatval($wooshc_condition[$key_condicion]["resultado_costo"]) '.$and_or.' ($total_peso>=$split_peso[0] && $total_peso<=$split_peso[1]))
                                {
                                  $val = $wooshc_condition[$key_condicion]["precios"];
                                  $express = $wooshc_condition[$key_condicion]["express"];
                                  $programado = $wooshc_condition[$key_condicion]["programado"];

                                  //$val = $total_peso;
                                  $exist_distrito = 1;
                                  $distrito_break_opcion = $wooshc_condition[$key_condicion]["precios"];
                                  $express_break_opcion = $wooshc_condition[$key_condicion]["express"];
                                  $programado_break_opcion = $wooshc_condition[$key_condicion]["programado"];


                              };';

                            }else{
                              $code='if($amount '.$compare.' floatval($wooshc_condition[$key_condicion]["resultado_costo"]))
                                {
                                  $val = $wooshc_condition[$key_condicion]["precios"];
                                  $express = $wooshc_condition[$key_condicion]["express"];
                                  $programado = $wooshc_condition[$key_condicion]["programado"];

                                  //$val = $total_peso;
                                  $exist_distrito = 1;
                                  $distrito_break_opcion = $wooshc_condition[$key_condicion]["precios"];
                                  $express_break_opcion = $wooshc_condition[$key_condicion]["express"];
                                  $programado_break_opcion = $wooshc_condition[$key_condicion]["programado"];


                              };';
                            }
                            echo eval($code);
                     
                    }//cierre del if
                    


                }//cierre del foreach secun dario
              }//cierre del foreach*/


              if($distrito_break_opcion != '')
              {
                  $val = $distrito_break_opcion;
              }


              //--------------------BUSCAREMOS EL DISTRITO Y SUS GATEWAYS CORRESPONDIENTES-------------------
              $wooshc_option_pagos = get_option('wooshc_option_pagos',true);
              error_log('DEBUG WHEN MODIFE SESSION PAYMNENT METHOD');              
              /*
              error_log(serialize($wooshc_option_pagos));
              //options principal para buscarl a lista de distritos
              foreach ($wooshc_option_pagos as $key_option_pago => $value) {
                //buscamos los distritos dentro del arreglo distritos
                foreach ($wooshc_option_pagos[$key_option_pago]['distritos'] as $key_option_pago2 => $value2) { 
                    if( ($wooshc_option_pagos[$key_option_pago]['distritos'][$key_option_pago2] == $_POST['district']) 
                          && ($wooshc_option_pagos[$key_option_pago]['regiones']==$_POST['region']) 
                            && ($wooshc_option_pagos[$key_option_pago]['provincias']==$_POST['province']) ) {
                      $mygateways = $wooshc_option_pagos[$key_option_pago]['gateways'];
                      $exist_distrito = 1;
                      break;
                    }
                }//cierre foreach secundario
              }//cieerre foreach de wooshc_option
              */
              //New loop by VinhDQ 08.02.2022
              //Tìm kiếm trong danh sách dữ liệu, nếu như không setup tới district thì chỉ lấy tới region
              //Tìm kiếm lần 1, chỉ xét tới các cấu hình tới district
              $founDistrict = false;
              foreach($wooshc_option_pagos as $key1 => $item1) {
                if($item1['regiones'] == $_POST['region'] && $item1['provincias'] == $_POST['province']) {
                  if(count($item1['distritos']) > 0) {
                    // Có cấu hình tới district, so sánh tiếp tới district
                    foreach($item1['distritos'] as $key2 => $province) {
                      if($province == $_POST['district']) {
                        $mygateways = $item1['gateways'];
                        $exist_distrito = 1;
                        $founDistrict = true;
                        break;
                      }
                    }
                  }
                }
              }
              //Tìm kiếm lần 2, nếu không có cấu hình tới district thì tìm kiếm cấu hình tới province
              if(!$founDistrict) {
                foreach($wooshc_option_pagos as $key1 => $item1) {
                  if($item1['regiones'] == $_POST['region'] && $item1['provincias'] == $_POST['province']) {
                    if(count($item1['distritos']) == 0) {                      
                      //Không cấu hình tới district
                      $mygateways = $item1['gateways'];
                      $exist_distrito = 1;
                      break;
                    }
                  }
                }
              }

              error_log(serialize($mygateways));


          }


       

          //si no existe un distrito de esta forma
          if($exist_distrito==0)
          {
            $distric_no_configure = get_option('distric_no_configure',true);
            $distric_no_configure = isset($distric_no_configure) ? $distric_no_configure : '';
            if($distric_no_configure == 1){
              $distric_no_configure = '';
            }
            wc_add_notice (__($distric_no_configure), 'error');
          }

          session_start();
          $_SESSION['val'] = $val;
          $_SESSION['express'] = $express;
          $_SESSION['programado'] = $programado;

          if($exist_distrito!=0){
            $_SESSION['gate'] = 'predefined';
            $_SESSION['array_gateways'] = $mygateways;
          }else{
            $_SESSION['gate'] = 'defaults';
          }
          $_SESSION['exist_distrito'] = $exist_distrito;
        }

        //vamos a aplicar un filtro por ajax

            //mostramos los mensajes settings aqui
             $mmyo_settings = get_option('mmyo_settings',true);
             $dia_semana = get_dia_semana();
             $dia_regular = $mmyo_settings['horario_regular'][$dia_semana];
             $dia_express = $mmyo_settings['horario_express'][$dia_semana];
             $dia_programado = $mmyo_settings['horario_programado'][$dia_semana];
           // $programado = $mmyo_settings['mmyo_mensaje_programado'][$dia_semana];


             //Estos son los horarios para recojo en ALMACEN
             $dias_recojo_almacen = $mmyo_settings['recojo_almacen_desde'][$dia_semana];



             //vamos a guardar las opciones
             $arreglo_opciones = array();
             //si es igual a YES es por que hoy esta disponible esta opción
             if($dia_regular == 'yes' && $dia_express == 'yes')
             {
              if($express!='')
              {

                 $args = array(
                 'type' => 'radio',
                 'class' => array('select_metodos','form-row-wide', 'update_totals_on_change' ),
                 'options' => array(
                    "$val-Regular" => 'Regular ( S/'.$val.' )   / '.$mmyo_settings['mmyo_mensaje_regular'].'',
                    "$express-Express" => 'Express ( S/'.$express.' )  / '.$mmyo_settings['mmyo_mensaje_express'].'',
                    "$programado-Programado" => 'Envíos Ya ! ( S/'.$programado.' ) / '.$mmyo_settings['mmyo_mensaje_programado'].'',
                 ),
                 'default' => $chosen
                 );
               }else{
                
                   $args = array(
                   'type' => 'radio',
                   'class' => array('select_metodos','form-row-wide', 'update_totals_on_change' ),
                   'options' => array(
                      "$val-Regular" => 'Regular ( S/'.$val.' )   / '.$mmyo_settings['mmyo_mensaje_regular'].'',
                      "$programado-Programado" => 'Envíos Ya !  ( S/'.$programado.' ) /'.$mmyo_settings['mmyo_mensaje_programado'].'',
                   ),
                   'default' => $chosen
                   );
               }

             }
             if($dia_regular != 'yes' && $dia_express == 'yes')
             {
              if($express!=''){
                 $args = array(
                 'type' => 'radio',
                 'class' => array('select_metodos','form-row-wide', 'update_totals_on_change' ),
                 'options' => array(
                    "$express-Express" => 'Express ( S/'.$express.' )  / '.$mmyo_settings['mmyo_mensaje_express'].'',
                    "$programado-Programado" => 'Envíos Ya !  ( S/'.$programado.' ) /'.$mmyo_settings['mmyo_mensaje_programado'].'',
                 ),
                 'default' => $chosen
                 );
               }else{

                    $args = array(
                   'type' => 'radio',
                   'class' => array('select_metodos','form-row-wide', 'update_totals_on_change' ),
                   'options' => array(
                      "$val-Regular" => 'Regular ( S/'.$val.' )   / '.$mmyo_settings['mmyo_mensaje_regular'].'',
                      "$programado-Programado" => 'Envíos Ya ! ( S/'.$programado.' ) /'.$mmyo_settings['mmyo_mensaje_programado'].'',
                   ),
                   'default' => $chosen
                   );

               }
             }
            
             if($dia_regular == 'yes' && $dia_express !='yes')
             {
               $args = array(
               'type' => 'radio',
               'class' => array('select_metodos','form-row-wide', 'update_totals_on_change' ),
               'options' => array(
                  "$val-Regular" => 'Regular ( S/'.$val.' )   / '.$mmyo_settings['mmyo_mensaje_regular'].'',
                  "$programado-Programado" => 'Envíos Ya ! ( S/'.$programado.' ) /'.$mmyo_settings['mmyo_mensaje_programado'].'',
               ),
               'default' => $chosen
               );
             } 

            if($dia_regular != 'yes' && $dia_express !='yes')
             {
               $args = array(
               'type' => 'radio',
               'class' => array('select_metodos','form-row-wide', 'update_totals_on_change' ),
               'options' => array(
                  "$programado-Programado" => 'Envíos Ya ! ( S/'.$programado.' ) /'.$mmyo_settings['mmyo_mensaje_programado'].'',
               ),
               'default' => $chosen
               );
             } 


            if($programado == '')
            {
                $precioprogramado = "$programado-Programado";
                unset($args['options'][$precioprogramado]);
            }
              

          //vamos a ver si cumple con la condicion de traerlo segun la provincia y distrito
          $provincias_recojo_almacen = $mmyo_settings['provincias_recojo_almacen'];
          $distrito_recojo_almacen = $mmyo_settings['distrito_recojo_almacen'];
          $provincia_temporal = isset($_POST['province']) ? $_POST['province'] : '';
          $distrito_temporal = isset($_POST['district']) ? $_POST['district'] : '';
          $encontro_provincia = false;


          //esto es para recojo en tienda verificaremos si se cumple con los registros en el departamento de lima
          if($provincia_temporal != '')
          { 
            if(in_array($provincia_temporal, $provincias_recojo_almacen))
            {
              $args['options']['0-recojo-en-almacen'] = 'Recojo en Almacen ';
              $encontro_provincia = true;
            }else{
              unset($args['options']['0-recojo-en-almacen']);
            }

          }

          //si no es un arreglo
          if(!isset($distrito_recojo_almacen[$provincia_temporal]) || !is_array($distrito_recojo_almacen[$provincia_temporal]))
          {
            $distrito_recojo_almacen[$provincia_temporal] = array();
          }

          //aqui arriba ya buscamos los registros de lo que es en regiones, ahora buscaremos solo en distritos
          if($distrito_temporal != '')
          {
            if(in_array($distrito_temporal, $distrito_recojo_almacen[$provincia_temporal]))
            {
              $args['options']['0-recojo-en-almacen'] = 'Recojo en Almacen ';
            }else{
                //verificaremos que si esten  DISTRITOS en esta provincia registrados
                 if(count($distrito_recojo_almacen[$provincia_temporal])>0)
                 {
                      unset($args['options']['0-recojo-en-almacen']);

                 }else{

                      if($encontro_provincia == false){
                      }else{
                         $args['options']['0-recojo-en-almacen'] = 'Recojo en Almacen ';
                      }
                }
            }
          }




          //=================================================================
          //lo coloramemos por defecto ya
          $args['options']['0-recojo-en-almacen'] = 'Recojo en Tienda . El Recojo es previa coordinación por correo o nuestro WhatsApp.';


            $text_html = '';
             echo '<div id="checkout-radio">';
            if(isset($args['options']['-Regular']))
            {

            }else{
               $text_html = '<h3>Método de envío</h3>';

            }

            if(isset($args['options']['0-recojo-en-almacen']))
            {
               $text_html = '<h3>Método de envío</h3>';
            }

            echo $text_html;
            woocommerce_form_field( 'radio_choice', $args, $chosen );
             
             echo '</div>';


        wp_die();

}



add_action('woocommerce_cart_calculate_fees', 'wpi_add_ship_fee');
function wpi_add_ship_fee() {
      @session_start();
       global $post;

      if(isset($_SESSION['val'])){
        $customshipcost = $_SESSION['val'];
      //  WC()->cart->add_fee('Envio: ', $customshipcost);
      }
 }

function maybe_disable_other_gateways( $available_gateways ) {
     if(is_checkout()){
        @session_start(); 
        global $woocommerce;
        if(isset($_SESSION['gate']) && $_SESSION['gate']=='defaults'){
          //mostraremos los que vamos a quitar
          $gateways_default = get_option('wooshc_gateways_default',true);
          if(is_array($gateways_default)){
            foreach ($gateways_default as $gateway => $value) {
              //eliminar los tildados
              if(isset( $available_gateways[$gateways_default[$gateway]] )) {
                unset( $available_gateways[$gateways_default[$gateway]]);
              }
            }
          }
          //este else es si encontramos el distrito
        }
        else
        { 
         if(is_array( $_SESSION['array_gateways']))
            {
              $gateways_default = $_SESSION['array_gateways'];
              foreach ($gateways_default as $gateway => $value) 
              {
                //eliminar los tildados
                if(isset( $available_gateways[$gateways_default[$gateway]] )) {
                   unset( $available_gateways[$gateways_default[$gateway]]);
                }
              }
            }
        }

      }
      return $available_gateways;
}
//add_action( 'woocommerce_available_payment_gateways', 'maybe_disable_other_gateways');

//VINHDQ FIX PAYMENT GATEWAY
add_filter( 'woocommerce_available_payment_gateways', 'filter_woocommerce_available_payment_gateways', 10, 1 );

function filter_woocommerce_available_payment_gateways( $available_gateways ) { 

    if ( ! ( is_checkout_pay_page() ) ) {

          error_log(serialize($_SESSION['gate']));
          error_log(serialize($_SESSION['array_gateways']));
          $gateways_default = null;
          if(isset($_SESSION['gate']) && $_SESSION['gate']=='defaults'){
            $gateways_default = get_option('wooshc_gateways_default',true);
          }else {
            $gateways_default = $_SESSION['array_gateways'];
          }
          // If so, disable the gateways
          if ( $gateways_default ) {
              foreach ( $available_gateways as $id => $gateway ) {
                  if ( !in_array( $id, $gateways_default ) ) {
                      unset( $available_gateways[$id] );
                  }
              }
          }
          return $available_gateways;
    }
      else { 
        return $available_gateways;
    }
}





//AQUI VAMOS A VALIDAR QUE LA FECHA DE ENTREGA EXISTA SI ESTAMOS EN FECHA PROGRAMADA
add_action( 'woocommerce_checkout_process', 'validate_fecha_entrega_mmyo' );  
function validate_fecha_entrega_mmyo() { 
    $metodo_envio = WC()->session->get('radio_chosen'); 

  //Esto es para ENVIOS PROGRAMADOS
  if(isset($_POST['horario_activo']) && $_POST['horario_activo'] == 'yes')
  {
      if(empty($_POST['mmyo_dia_semana']) || empty($_POST['mmyo_hora_semana'])){
        wc_add_notice( 'La fecha de entrega es OBLIGATORIA', 'error' );  
      }
  }


  //Esto es para ENVIOS en recojo ALMACEN
  if(isset($_POST['horario_activo_recojo_almacen']) && $_POST['horario_activo_recojo_almacen'] == 'yes')
  {
      if(empty($_POST['mmyo_dia_semana_recojo_almacen']) || empty($_POST['mmyo_hora_semana_recojo_almacen'])){
        wc_add_notice( 'La fecha de recojo en Almacén es OBLIGATORIA', 'error' );  
      }
  }



  //validar que escoja un metodo de envio es obligatorio
  if(!isset($metodo_envio) || empty($metodo_envio))
  {
     wc_add_notice( 'Debe escoger un metodo de envío', 'error' );  
  } 
}



//AQUI VAMOS A GUARDAR EL CAMPO DE FECHA DE ENTREGA Y HORARIO ESTO ES NUEVO 2020 
add_action( 'woocommerce_checkout_update_order_meta', 'mmyo_fecha_entrega_valor' );
function mmyo_fecha_entrega_valor( $order_id ) { 

  //Aqui guardamos si Existe el Horario Activo pero de ENVIOS PROGRAMADO
    if (isset($_POST['horario_activo']) && $_POST['horario_activo'] == 'yes')
    {
      $fecha_entrega = $_POST['mmyo_dia_semana'].' '.$_POST['mmyo_hora_semana'];
      //guardamos el valor de la fecha de entrega
      update_post_meta( $order_id, 'mmyo_fecha_entrega', esc_attr($fecha_entrega));
    }


  //Aqui guardamos si Existe el Horario Activo pero de Recojo en Tienda
  if (isset($_POST['horario_activo_recojo_almacen']) && $_POST['horario_activo_recojo_almacen'] == 'yes')
  {
      $fecha_entrega = $_POST['mmyo_dia_semana_recojo_almacen'].' '.$_POST['mmyo_hora_semana_recojo_almacen'];
      //guardamos el valor de la fecha de entrega
      update_post_meta( $order_id, 'mmyo_fecha_entrega_recojo_almacen', esc_attr($fecha_entrega));
  }

}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'mmyo_fecha_entrega_checkout_field_order', 10, 1 );
   
function mmyo_fecha_entrega_checkout_field_order( $order ) 
{    
   $order_id = $order->get_id();
   //ESTA FECHA ES PARA EL ENVIO QUE ES PROGRAMADO
   if ( get_post_meta($order_id,'mmyo_fecha_entrega',true))
   {
    //id comuna
    $fecha_entrega = get_post_meta($order_id,'mmyo_fecha_entrega',true);
    echo '<p><strong>Fecha de Entrega:</strong> '.$fecha_entrega. '</p>';
  }

  //ESTA FECHA ES PARA EL ENVIO POR RECOJIDA EN ALMACEN
  if ( get_post_meta($order_id,'mmyo_fecha_entrega_recojo_almacen',true))
   {
    //id comuna
    $fecha_entrega = get_post_meta($order_id,'mmyo_fecha_entrega_recojo_almacen',true);
    echo '<p><strong>Fecha de recojo en Almacén:</strong> '.$fecha_entrega. '</p>';
  }
  

}
 
//=======================
add_action( 'woocommerce_email_before_order_table', 'mmyo_fecha_entrega_new_checkout_field_emails', 20, 4 );
function mmyo_fecha_entrega_new_checkout_field_emails( $order, $sent_to_admin, $plain_text, $email ) 
{
    if (get_post_meta( $order->get_id(), 'mmyo_fecha_entrega',true)){
    $fecha_entrega = get_post_meta( $order->get_id(), 'mmyo_fecha_entrega',true);
     
?>
  <div style="color:red;font-weight:700;">
    <?php esc_html_e( 'Fecha de Entrega:', 'woocommerce' ); ?>
      <strong><?php echo $fecha_entrega; ?></strong>
    <h3>¡La fecha la cual podras retirar el producto!</h3>
  </div><br />


<?php 
  }

  //ESTA ES PARA RECOJO EN ALMACEN
   if (get_post_meta( $order->get_id(), 'mmyo_fecha_entrega_recojo_almacen',true)){
    $fecha_entrega = get_post_meta( $order->get_id(), 'mmyo_fecha_entrega_recojo_almacen',true);
  ?>
    <div style="color:red;font-weight:700;">
      <?php esc_html_e( 'Fecha de recojo en Almacén: ', 'woocommerce' ); ?>
      <strong><?php echo $fecha_entrega; ?></strong>
      <h3>¡La fecha la cual podras retirar el producto!</h3>
    </div><br />


  <?php 
    }

}




 //------------------------------INCLUIR NUEVOS CAMPOS PARA EL FORMULARIO DE REGISTRO---------------
 add_action( 'woocommerce_register_form', 'wooc_extra_register_fields');
 //agregar campos nuevos para el formulario de registro
 function wooc_extra_register_fields() {
    require_once wooshc_path.'includes/view/view_custom_register.php';
 }


//accion para realizar al momento de registrar un usuario
add_action( 'user_register', 'myplugin_registration_save', 10, 1 );
function myplugin_registration_save($user_id){

   //esto servira para el checkout
   if(!is_user_logged_in())
    {

        //AHORA AQUI SIMPLEMENTE ACTUALIZAREMOS LOS DATOS DEL USUARIO
        /*vamos a obtener los datos del pedido*/
         if($_POST['action_form'] == 'billing'){  

            if(isset( $_POST['billing_company'])){
                update_user_meta( $user_id, 'billing_company', sanitize_text_field($_POST['billing_company']));
            }
          
            //registrar nuestras regiones
            update_user_meta($user_id,'wooshc_region',$_POST['region']);
           /* update_user_meta($user_id,'comprobante_user',$_POST['comprobante']);*/
            update_user_meta($user_id,'wooshc_provincia',$_POST['provincia']);
            update_user_meta($user_id,'wooshc_distrito',$_POST['distrito']);
        }
          //en el formulario de direccion diferente
          if($_POST['action_form']=='shipping'){ 

              if( isset( $_POST['billing_company'] ) )
                  update_user_meta( $user_id, 'billing_company', sanitize_text_field( $_POST['billing_company2']));

          
                update_user_meta($user_id,'wooshc_region',$_POST['region2']);
                update_user_meta($user_id,'wooshc_provincia',$_POST['provincia2']);
                update_user_meta($user_id,'wooshc_distrito',$_POST['distrito2']);
                update_user_meta($user_id,'comprobante_user',$_POST['comprobante2']);
          }
    }//cierre del if si esta logueado o no


}


/*VAMOS A GUARDAR LOS DATOS DE LOS DETALLES DE CUENTA como sexo del bebe , rut , entre otros*/
add_action( 'woocommerce_save_account_details', 'save_custom_account_details', 12, 1 );
function save_custom_account_details( $user_id ) {

    if( isset( $_POST['billing_company'] ) )
        update_user_meta( $user_id, 'billing_company', sanitize_text_field( $_POST['billing_company'] ) );
}


//Configurar States
add_filter( 'woocommerce_states', 'custom_woocommerce_states' );
function custom_woocommerce_states( $states ) {
    
  $regions = wooshc_get_regions();
  $states['PE'] = array();
  foreach ($regions as $data) {
    $states['PE'][$data->id] = $data->name;
  }

    return $states;
}

	
