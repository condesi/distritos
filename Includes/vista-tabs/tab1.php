		<h2 style="padding:10px; background-color:#CC99C2; color:white;" class="tab_costo_por_distritos tab-mamitayyo">Configuración de Costos por distritos</h2>

					<hr />

					<div class="pagination" id="paginacion_tab1">
						<ol class="numbers"></ol>
					</div>

					<!--VARIABLES TEMPORALES-->

					<input type="hidden" name="regiones_general[]">

					<input type="hidden" name="provincias_general[]">

					<input type="hidden" name="distritos_text_general[]">

					<input type="hidden" name="regiones_text_general[]">

					<input type="hidden" name="prov_text_general[]">

					<input type="hidden" name="posicion_distritos_general[]" value="">

					

					<table width="100%" id="mytable" agregado="general" class="tab_costo_por_distritos tab-mamitayyo display">

						<thead>

							<tr>

								<td>Precio Regular</td>

								<td>Precio Express</td>

								<td>Precio Programado</td>

								<th>Pais/Estado</th>

								<th>Provincia</th>

								<th>Distrito</th>

								<td></td>

							</tr>

						</thead>

						<tbody>



							<tr class="tr_padre">

							<td>

								<input type="text" class="mp_fee_input" name="precios[]"/>

							</td>

							<td>

								<input type="text" class="mp_fee_input_express" name="express[]"/>

							</td>

							<td>

								<input type="text" class="mp_fee_input_programado" name="programado[]"/>

							</td>

								<td>

									<?php

										$regions = wooshc_get_regions();

									 ?>

									<select id="region" name="regiones[]">

										<option value="">Seleccione el departamento</option>

										<?php 

											foreach ($regions as $data) {

										 ?>

										 	<option value="<?php echo $data->id; ?>"><?php echo $data->name; ?></option>

										 <?php  } ?>

									</select>

								</td>

								<td>

									<select id="provincia" name="provincias[]">

										<option value="">Seleccione la provincia</option>

									</select>

								</td>

								<td>

									<select name="distritos[]"  id="distrito" class="distrito js-example-basic-multiple" multiple="multiple" style="width: 100% !important;">

										<option value="">Seleccione el distrito</option>

									</select>

								</td>

								<td><button type="button" id="wc_mp_mp_add" class="wc_mp_mp_add">ADD</button></td>

							</tr>

							<?php 

								$recoveredData = file_get_contents(wooshc_path.'includes/wooshc_option.txt');

								//buscamos en el archivo los datos a ejecutar

								$wooshc_option = unserialize($recoveredData);

								if(count($wooshc_option) <=1)

								{

									$wooshc_option = get_option('wooshc_option',true);

								}

								



								$i_general = 0;

								foreach ($wooshc_option as $key => $value) {

							 		$i_general+=1;

							 ?>

							 <tr>

								<td>

									<!--SACAMOS LA POSICION GENERAL-->

									<input type="hidden" name="posicion_distritos_general[]" value="<?php echo $i_general; ?>">

									<input type="text" class="mp_fee_input mp_fee_input_foreach" name="precios[]" value="<?php echo  $wooshc_option[$key]['precios']; ?>"/>

								</td>

								<!-- nuevos campos de precios-->

								<td>

									<input type="text" class="mp_fee_input_express" name="express[]" value="<?php echo  $wooshc_option[$key]['express']; ?>"/>

								</td>

								<td>

									<input type="text" class="mp_fee_input_programado" name="programado[]" value="<?php echo  $wooshc_option[$key]['programado']; ?>" />

								</td>



								<td>

									<input type="hidden" keydata="regiones" name="regiones_general[]" value="<?php echo $wooshc_option[$key]['regiones']; ?>">

									<input type="text" keydata="regiones_text" name="regiones_text_general[]" value="<?php echo $wooshc_option[$key]['regiones_text']; ?>">

								</td>

								<td>

									<input type="hidden" keydata="provincias" name="provincias_general[]" value="<?php echo $wooshc_option[$key]['provincias']; ?>">

									<input type="text" keydata="prov_text" name="prov_text_general[]" value="<?php echo $wooshc_option[$key]['prov_text']; ?>">

								</td>

								<td>

									<input type="hidden" keydata="distritos_text" name="distritos_text_general[]" value="<?php echo $wooshc_option[$key]['distritos_text']; ?>">

									

									<select keydata="distritos" name="distritos_general_<?php echo $i_general; ?>[]"  id="distrito" class="distrito js-example-basic-multiple" multiple="multiple"  style="width: 100% !important;">

										<?php 

										$list_distritos = wooshc_get_districts_province($wooshc_option[$key]['provincias'],$wooshc_option[$key]['regiones']);

										//$distritos_text =  explode(',',$wooshc_option[$key]['distritos_text'][0]); 

										//foreach ($wooshc_option[$key]['distritos'] as $key2 => $value2) { ?>

										<?php foreach ($list_distritos as $data) { ?>	

											<!--<option selected="selected" value="<?php echo $wooshc_option[$key]['distritos'][$key2]; ?>"><?php echo $distritos_text[$key2]; ?></option>-->

											<option  <?php echo selecteds($data->id,$wooshc_option[$key]['distritos']); ?>  value="<?php echo $data->id; ?>"><?php echo $data->name; ?></option>

										<?php 

											} 

											

										?>

									</select>

								</td>

								<td><button type="button" id="wc_mp_mp_remove" class="wc_mp_mp_remove">REMOVE</button></td>

							</tr>

							 <?php 

							 	}

							 ?>

						</tbody>

					</table>

					<hr>

		

