	<h2 style="padding:10px; background-color:#CC99C2; color:white;" class="tab_configuracion_metodo_pago tab-mamitayyo">Configuración de metodos de pago (Ocultar/Mostrar)</h2>

						<div class="pagination" id="paginacion_tab2">
							<ol class="numbers"></ol>
						</div>

						<p class="tab_configuracion_metodo_pago tab-mamitayyo"><b>PARA OCULTAR METODO DE PAGO DEBE SER SELECCIONADO<b></p>
						<input type="hidden" name="posicion_distritos_pagos[]" value="">
						<input type="hidden" name="regiones_pagos[]">
						<input type="hidden" name="provincias_pagos[]">
						<input type="hidden" name="distritos_text_pagos[]">
						<input type="hidden" name="regiones_text_pagos[]">
						<input type="hidden" name="prov_text_pagos[]">
						
						<!--tendra dos secciones-->
						<div style="float:left; width:80%;">
							<table width="100%" id="mytable2" agregado="pagos" class="tab_configuracion_metodo_pago tab-mamitayyo display">
							<thead>
								<tr>
									<td>Gateways</td>
									<th>Pais/Estado</th>
									<th>Provincia</th>
									<th>Distrito</th>
									<td></td>
								</tr>
							</thead>
							<tbody>	
								<tr class="tr_padre">
								<td style="background-color:white;">
									<?php 
										//Con esto nos traemos todas los metodos de pagos		
										$available_payment_methods = WC()->payment_gateways->get_available_payment_gateways();
										foreach ($available_payment_methods as $payment => $value) {
											echo $available_payment_methods[$payment]->title;
											echo '<hr>';
										?>
											<p>
												<input type="checkbox" class="wp_payment" name="wp_payment[]" value="<?php echo $payment; ?>"><span><?php echo $available_payment_methods[$payment]->title; ?></span><br>
											</p>
										<?php
										}
									?>
								</td>
									<td>
										<?php
											$regions = wooshc_get_regions();
										 ?>
										<select id="region" name="regiones[]">
											<option value="">Seleccione el departamento</option>
											<?php 
												foreach ($regions as $data) {
											 ?>
											 	<option value="<?php echo $data->id; ?>"><?php echo $data->name; ?></option>
											 <?php  } ?>
										</select>
									</td>
									<td>
										<select id="provincia" name="provincias[]">
											<option value="">Seleccione la provincia</option>
										</select>
									</td>
									<td>
										<select name="distritos[]"  id="distrito" class="distrito js-example-basic-multiple" multiple="multiple"  style="width: 100% !important;">
											<option value="">Seleccione el distrito</option>
										</select>
									</td>
									<td><button type="button" id="wc_mp_mp_add" class="wc_mp_mp_add">ADD</button></td>
								</tr>
								<?php 

									$recoveredData = file_get_contents(wooshc_path.'includes/wooshc_option_pagos.txt');
									$wooshc_option_pagos = unserialize($recoveredData);
									//buscamos en el archivo los datos a ejecutar

									if(count($wooshc_option_pagos) <= 1)
									{
										$wooshc_option_pagos = get_option('wooshc_option_pagos',true);
									}


							
									$i_pagos = 0;
									if($wooshc_option_pagos!=1){
									foreach ($wooshc_option_pagos as $key => $value) {
								 		$i_pagos+=1;
								 ?>
								 <tr>
									<td style="border:1px solid black;">
										<!--SACAMOS LA POSICION GENERAL-->
										<input type="hidden" name="posicion_distritos_pagos[]" value="<?php echo $i_pagos; ?>">	
										<?php 
											//Con esto nos traemos todas los metodos de pagos		
											$available_payment_methods = WC()->payment_gateways->get_available_payment_gateways();
											foreach ($available_payment_methods as $payment => $value) {
												//echo $available_payment_methods[$payment]->title;
												//echo '<hr>';
												if(is_array($wooshc_option_pagos[$key]['gateways'])){
													$checked =  checkeds($payment,$wooshc_option_pagos[$key]['gateways']);
												}else{
													$checked = '';
												}
												echo '<input type="checkbox" '.$checked.' class="wp_payment" name="wp_payment_'.$i_pagos.'[]" value="'.$payment.'"><b>'.$available_payment_methods[$payment]->title.'</b><br>';
											}
										?>

									</td>
									<td>
										<input type="hidden" keydata="regiones" name="regiones_pagos[]" value="<?php echo $wooshc_option_pagos[$key]['regiones']; ?>">
										<input type="text" keydata="regiones_text" name="regiones_text_pagos[]" value="<?php echo $wooshc_option_pagos[$key]['regiones_text']; ?>">
									</td>
									<td>
										<input type="hidden" keydata="provincias" name="provincias_pagos[]" value="<?php echo $wooshc_option_pagos[$key]['provincias']; ?>">
										<input type="text" keydata="prov_text" name="prov_text_pagos[]" value="<?php echo $wooshc_option_pagos[$key]['prov_text']; ?>">
									</td>
									<td>
										<input type="hidden" keydata="distritos_text" name="distritos_text_pagos[]" value="<?php echo $wooshc_option_pagos[$key]['distritos_text']; ?>">
										<select keydata="distritos" name="distritos_pagos_<?php echo $i_pagos; ?>[]"  id="distrito" class="distrito js-example-basic-multiple" multiple="multiple"  style="width: 100% !important;">
											<?php 
											$list_distritos = wooshc_get_districts_province($wooshc_option_pagos[$key]['provincias'],$wooshc_option_pagos[$key]['regiones']);
											//$distritos_text =  explode(',',$wooshc_option_pagos[$key]['distritos_text'][0]); 
											//foreach ($wooshc_option_pagos[$key]['distritos'] as $key2 => $value2) { ?>
											<?php foreach ($list_distritos as $data) { ?>	
												<!--<option selected="selected" value="<?php echo $wooshc_option_pagos[$key]['distritos'][$key2]; ?>"><?php echo $distritos_text[$key2]; ?></option>-->
												<option  <?php echo selecteds($data->id,$wooshc_option_pagos[$key]['distritos']); ?>  value="<?php echo $data->id; ?>"><?php echo $data->name; ?></option>
											<?php 
												} 
												
											?>
										</select>
									</td>
									<td><button type="button" id="wc_mp_mp_remove" class="wc_mp_mp_remove">REMOVE</button></td>
								</tr>
								 <?php 
								 	}
								 }//cierre del foreach
								 ?>
							</tbody>
						</table>
					</div>
					<div style="float:right; width:18%;"  class="tab_configuracion_metodo_pago tab-mamitayyo">
						<h3>Gateways por defecto ocultos</h3>
						<p style="background-color:#AE83A5; border:2px solid #CC99C2; color:white; padding:10px;">
						<?php 
											//Con esto nos traemos todas los metodos de pagos		
								$available_payment_methods = WC()->payment_gateways->get_available_payment_gateways();
								$gateways_array = get_option('wooshc_gateways_default');
								//print_r($gateways_array);
								foreach ($available_payment_methods as $payment => $value) {
									//echo $available_payment_methods[$payment]->title;
									//echo '<hr>';
									$checked='';
									if(isset($gateways_array[0])){
										if(array_search($payment, $gateways_array)===false){
											$checked = '';
										}else{
											$checked = 'checked=checked';
										}
									}

									echo '<input type="checkbox" '.$checked.' class="wp_payment" name="wp_payment_default[]" value="'.$payment.'"><b>'.$available_payment_methods[$payment]->title.'</b><br>';
								}
							?>
						</p>
					</div>
					<div style="clear:both;"></div>