 <?php 
	global $wpdb;
	$table_name = "wooshc_districts";

	if(isset($_POST['cargar_sql']))
	{	

		if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) 
		{
			require_once wooshc_path.'includes/sqlubigeo.php';
			global $wpdb;
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	 		//insetamos la tabla automaticamente
	 		dbDelta($sql_region);
	 		dbDelta($sql_provincia);
	 		dbDelta($sql_distrito);
 			echo "<h3>Base de datos Cargada EXITOSAMENTE<h3>";

 		}else{
 			echo "<h3>Ya existen las tablas de ubigeo en la base de datos<h3>";
 		}
	}

	if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) 
	{

	?>
		<form action="" method="POST">	
		<p>Es nesecario Cargar todas las Regiones/Provincias/Distritos <input type="submit" value="Cargar Datos" name="cargar_sql" class="button button-primary"></p>
		</form>	
		<?php
	}
	function selecteds($dataid, $distritos)
	{
			$string = "";
			//if(count($distritos)>1){
			if(is_array($distritos)){
				foreach ($distritos as $key => $value) {
					if($dataid == $distritos[$key]){
						$string = "selected=selected";
						break;
					}
				}
				}
			//}else{
			//	if($dataid == $distritos)
			//	{
			//		$string = "selected=selected";
			//	}
			//}
			return $string;
	}
	//buscareos todos los gateways seleccionados
	function checkeds($dataid,$gateways)
	{
		$string = "";
			//if(count($distritos)>1){
				foreach ($gateways as $key => $value) {
					if($dataid == $gateways[$key]){
						$string = "checked=checked";
						break;
					}
				}
			//}else{
			//	if($dataid == $distritos)
			//	{
			//		$string = "selected=selected";
			//	}
			//}
			return $string;
	}	
?>		
			<style type="text/css">
				.tab_costo_por_distritos{
					display: block;
				}
				.tab_configuracion_metodo_pago{
					display: none;
				}
				.tab_condiciones_costo{
					display: none;
				}
				.select2-selection__rendered{
					width: 100% !important;
				}
				.dataTables_wrapper .dataTables_length select{
					min-width: 100px !important;
				}
				#mytable2_wrapper{
					display: none;
				}
				#mytable_condicion_wrapper{
					display: none;
				}

				/* Pagination. */
				.pagination {background: #333;padding: 1rem;margin-bottom: 1rem;text-align: center;display: flex;justify-content: center; display: none;}
				#paginacion_tab1{display: block;}
				.numbers {padding: 0;margin: 0 2rem;list-style-type: none;display: flex;}

				.numbers li a {color: #fff;padding: .5rem 1rem;text-decoration: none;opacity: .7;}

				.numbers li a:hover {opacity: 1;}

				.numbers li a.active {opacity: 1;background: #fff;color: #333;}


			</style>


			 <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
  
    		<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
    		 

						<script type="text/javascript">
						    jQuery(document).ready(function($) 
						    {
						    

						    		var rowsPerPage = 25;
									var rows = $('#mytable tbody tr');
									var rowsCount = rows.length;
									var pageCount = Math.ceil(rowsCount / rowsPerPage) - 1; // avoid decimals
									var numbers = $("#paginacion_tab1 .numbers");
									// Generate the pagination.
									for (var i = 0; i < pageCount; i++) {
										numbers.append('<li><a href="#">' + (i+1) + '</a></li>');
									}
				
									// Mark the first page link as active.
									$('#paginacion_tab1 .numbers li:first-child a').addClass('active');

									// Display the first set of rows.
									displayRows(1);
									
									// On pagination click.
									$('#paginacion_tab1 .numbers li a').click(function(e) {
										var $this = $(this);
										
										e.preventDefault();
										
										// Remove the active class from the links.
										$('#paginacion_tab1 .numbers li a').removeClass('active');
										
										// Add the active class to the current link.
										$this.addClass('active');
										
										// Show the rows corresponding to the clicked page ID.
										displayRows($this.text());
									});


									//TAB 2

									var rowsPerPage = 25;
									var rows = $('#mytable2 tbody tr');
									var rowsCount = rows.length;
									var pageCount = Math.ceil(rowsCount / rowsPerPage); // avoid decimals
									var numbers = $("#paginacion_tab2 .numbers");
									// Generate the pagination.
									for (var i = 0; i < pageCount; i++) {
										numbers.append('<li><a href="#">' + (i+1) + '</a></li>');
									}
				
									// Mark the first page link as active.
									$('#paginacion_tab2 .numbers li:first-child a').addClass('active');

									// Display the first set of rows.
									displayRows(1);
									
									// On pagination click.
									$('#paginacion_tab2 .numbers li a').click(function(e) {
										var $this = $(this);
										
										e.preventDefault();
										
										// Remove the active class from the links.
										$('#paginacion_tab2 .numbers li a').removeClass('active');
										
										// Add the active class to the current link.
										$this.addClass('active');
										
										// Show the rows corresponding to the clicked page ID.
										displayRows($this.text());
									});

										

									//TAB3
									var rowsPerPage = 25;
									var rows = $('#mytable_condicion tbody tr');
									var rowsCount = rows.length;
									var pageCount = Math.ceil(rowsCount / rowsPerPage); // avoid decimals
									var numbers = $("#paginacion_tab3 .numbers");
									// Generate the pagination.
									for (var i = 0; i < pageCount; i++) {
										numbers.append('<li><a href="#">' + (i+1) + '</a></li>');
									}
				
									// Mark the first page link as active.
									$('#paginacion_tab3 .numbers li:first-child a').addClass('active');

									// Display the first set of rows.
									displayRows(1);
									
									// On pagination click.
									$('#paginacion_tab3 .numbers li a').click(function(e) {
										var $this = $(this);
										
										e.preventDefault();
										
										// Remove the active class from the links.
										$('#paginacion_tab3 .numbers li a').removeClass('active');
										
										// Add the active class to the current link.
										$this.addClass('active');
										
										// Show the rows corresponding to the clicked page ID.
										displayRows($this.text());
									});

									// Function that displays rows for a specific page.
									function displayRows(index) {
										var start = (index - 1) * rowsPerPage;
										var end = start + rowsPerPage;
										
										// Hide all rows.
										rows.hide();
										
										// Show the proper rows for this page.
										rows.slice(start, end).show();
									}

						});
			</script> 


			<hr>
			<form  action="<?php echo admin_url('admin-post.php'); ?>" method="POST">
			<input type="hidden" name="action" value="wooshc_save_settings" id="wooshc_save_settings">
			
			<h2 style="padding:10px; background-color:#CC99C2; color:white;">Configuración Mensaje de muestra / Distrito no configurado</h2>
			<?php 
				$distric_no_configure = get_option('distric_no_configure',true);
				$distric_no_configure = isset($distric_no_configure) ? $distric_no_configure : '';
				if($distric_no_configure == 1){
					$distric_no_configure = '';
				}
			?>
			<input type="text" name="distric_no_configure" value="<?php echo $distric_no_configure; ?>" style="width:50%;">
			<hr>
			
			<!-- colocar los tabs-->
			<nav class="nav-tab-wrapper woo-nav-tab-wrapper">
			   <a href="javascript:void(0)" class="nav-tab" tab="tab_costo_por_distritos">Costos por distritos</a>		
			   <a href="javascript:void(0)" class="nav-tab" tab="tab_configuracion_metodo_pago">Configuración de metodos de pago</a>		
			   <a href="javascript:void(0)" class="nav-tab" tab="tab_condiciones_costo">Condiciones de costo por distrito</a>		
			</nav>



			<!-- SECCION COSTO POR DISTRITOS-->
				<?php require_once wooshc_path.'includes/vista-tabs/tab1.php';  ?>
			
			


					<!--#########################################################################.........................................OCULTAR Y MOSTRAR METODOS DE PAGO........................####################################################-->
				<?php require_once wooshc_path.'includes/vista-tabs/tab2.php';  ?>
					

					<!--################################################################.......................############################################################################################################################-->
		
			<?php require_once wooshc_path.'includes/vista-tabs/tab3.php';  ?>



			<!-- TAB CONDICIONES COSTO -->

					<!--..............................................CREAR CONDICIONES POR COSTO DISTRITOS..................................-->
					


			<table width="100%">
				<tbody id="wc_mp_mp_load_data"></tbody>
			</table>





			<?php submit_button(); ?>
			</form>
			<script type="text/javascript">
				jQuery(document).ready(function($) {

					//variable para obtener la lista de distritos por separado
					var $i_condicion = parseInt("<?php echo $i_condicion; ?>");
					var $i_general = parseInt("<?php echo $i_general; ?>")
					var $i_pagos = parseInt("<?php echo $i_pagos; ?>");

				    var wooshc_save_settings = jQuery("#wooshc_save_settings").val();
					  jQuery('.js-example-basic-multiple').select2();
						jQuery('body').on('DOMNodeInserted', 'select.js-example-basic-multiple', function () {
						     jQuery(this).select2();
						 });    
					jQuery(".js-example-basic-multiple").select2();
					//vamos a colocar al select los diverrsos campos
					jQuery(document).on('keyup','.mp_fee_input_condicion',function(){
			
					
					});

					//ESTA FUNCION ME PERMITIRA AGREGARLE UN VALOR DE POSICION para cada  campo de condicion o distritos
					function set_valor_campos(data)
					{
						valor = data;
						table.find("tr:last").find('td.condiciones select').each(function()
						{
							classitem = jQuery(this).attr('keydata');
							jQuery(this).attr('name',classitem+'_'+valor);
						});
						table.find("tr:last").find('td.condiciones input').each(function()
						{
								classitem = jQuery(this).attr('keydata');
								jQuery(this).attr('name',classitem+'_'+valor);
						});
						//los nocondicion 
						table.find("tr:last").find('td.nocondicion select').each(function()
						{
							classitem = jQuery(this).attr('keydata');
							jQuery(this).attr('name',classitem+'_'+valor+'[]');
						});
						table.find("tr:last").find('td.nocondicion input').each(function()
						{
								classitem = jQuery(this).attr('keydata');
								jQuery(this).attr('name',classitem+'_'+valor+'[]');
						});
					}

					jQuery(document).on('keyup','.mp_fee_input',function(){
						/*if(jQuery(this).hasClass('mp_fee_input_foreach')==false){
							valor = jQuery(this).val();
							jQuery(this).parent().parent().find('td select').each(function()
							{
								classitem = jQuery(this).attr('keydata');
								jQuery(this).attr('name',classitem+'_'+valor);
							});
							jQuery(this).parent().parent().find('td input').each(function()
							{
								if(jQuery(this).hasClass('mp_fee_input')==false){
									classitem = jQuery(this).attr('keydata');
									jQuery(this).attr('name',classitem+'_'+valor);
								}
							});
						}else{
							valor = jQuery(this).val();
							jQuery(this).parent().parent().find('td select').each(function()
							{
								classitem = jQuery(this).attr('keydata');
								jQuery(this).attr('name',classitem+'_'+valor+'[]');
							});
							jQuery(this).parent().parent().find('td input').each(function()
							{
								if(jQuery(this).hasClass('mp_fee_input')==false){
									classitem = jQuery(this).attr('keydata');
									jQuery(this).attr('name',classitem+'_'+valor+'[]');
								}
							});
						}//cierre del mp_fee_input foreach
						*/
					});


					//removr un elemento
					jQuery(document).on('click','.wc_mp_mp_remove',function(){
						jQuery(this).parent().parent().remove();
					});
				
					//---------------------------Agregar un elemento al hacer click------------
					jQuery(".wc_mp_mp_add").click(function(){

						table = jQuery(this).parent().parent().parent().parent();
						clone = table.find("tr.tr_padre").clone(true, true);
						clone.removeClass('tr_padre');
						//$(clone).insertAfter(".tab_costo_por_distritos tr:last");
						//console.log(clone.html());
						//obtenemos el valor de la tabla que estaremos usando la cual es  AGREGADO 
						valortabla = table.attr('agregado');
						if(valortabla!=''){
							valortabla+='';
						}


						//clone.find('td input[type=text]').val("jajaja");
						table.find('tbody').append('<tr class="temp_class">'+clone.html()+'</tr>');
						
						//table.find("tr:last").find('td input[type=text]').val(table.find("tr.tr_padre").find('td input[type=text]').val());

						table.find("tr:last").find('td input.mp_fee_input').val(table.find("tr.tr_padre").find('td input.mp_fee_input').val());
						table.find("tr:last").find('td input.mp_fee_input_express').val(table.find("tr.tr_padre").find('td input.mp_fee_input_express').val());
						table.find("tr:last").find('td input.mp_fee_input_programado').val(table.find("tr.tr_padre").find('td input.mp_fee_input_programado').val());
						
						table.find("tr:last").find('td button').text("REMOVE").addClass('wc_mp_mp_remove');
						
						//return false;

						//AQUI AGREGAMOS LOS VALORES
						//ESTO ES PARA PODER OBTENER LOS DISTRITOS
						if(table.attr('agregado')=='condicion'){
							$i_condicion = $i_condicion + 1;
							set_valor_campos($i_condicion);
						}
						if(table.attr('agregado')=='general'){  //Costos por distritos --> tab 1
							//alert($i_condicion);
							$i_general = $i_general+1;
						}
						if(table.attr('agregado')=='pagos'){ //payment
							$i_pagos = $i_pagos+1;
						}


						//OBTENEMOS EL VALOR Y LO PONDREMOS EN EL NAME
						if(table.attr('agregado')!='condicion'){
							tempfee = table.find("tr:last").find('td input.mp_fee_input').val();
						}
						if(table.attr('agregado')=='condicion'){
							tempfee = table.find("tr:last").find('td input.mp_fee_input_condicion').val();
						}

						tempregion = table.find("tr:last").find('td select#region');
						tempprovincia = table.find("tr:last").find('td select#provincia');
						tempdistrito = table.find("tr:last").find('td select#distrito');
						//hacemos el cambio
						tempregion.attr('name','regiones_'+valortabla+'[]');
						tempprovincia.attr('name','provincias_'+valortabla+'[]');
						
						//esto es para verifiar el estado de los distritos es decir colocarlos en su debida fila el listado que llevan
						if(table.attr('agregado')=='general'){
							tempdistrito.attr('name','distritos_'+valortabla+'_'+$i_general+'[]');
						}
						if(table.attr('agregado')=='condicion'){
							tempdistrito.attr('name','distritos_'+valortabla+'_'+$i_condicion+'[]');
						}
						if(table.attr('agregado')=='pagos'){
							tempdistrito.attr('name','distritos_'+valortabla+'_'+$i_pagos+'[]');
						}

						//}
						//-----------CIERRE: CON ESTO CUBRIMOS LA SELECCION COMPLETA----------

						//hacemos la copia de los selecteds en los options
						table.find("tr.tr_padre").find('td select').removeAttr('name');
						table.find("tr.tr_padre").find('td select').find('option').each(function(i)
						{
							//alert(table.find(this).text());
							//optionselect = table.find("tr.tr_padre").find('td select').find('option:selected').eq(i).text();
							optionselect = table.find("tr.tr_padre").find('td select').find('option').eq(i).attr('selected');

							if(table.find("tr.tr_padre").find('td select').find('option').eq(i).is(':selected') === true )
							{
								
								table.find("tr:last").find('td select').find('option').eq(i).attr('selected','selected');
							}

						
						});
						//esta opcion solo se ejcuta para lo de la condicion
						if(table.attr('agregado')=='pagos')
						{
							table.find("tr.tr_padre").find('td input[type=checkbox]').removeAttr('name');
							table.find("tr.tr_padre").find('td input[type=checkbox]').each(function(i)
							{
								//alert(table.find(this).text());
								optionsckecked = table.find("tr.tr_padre").find('td input[type=checkbox]').eq(i).attr('checked');
								
								if(table.find("tr.tr_padre").find('td input[type=checkbox]').eq(i).is(':checked') === true)
								{
									table.find("tr:last").find('td input[type=checkbox]').eq(i).attr('checked','checked');

								}
								
								name_temp = table.find("tr:last").find('td input[type=checkbox]').attr('class');
								table.find("tr:last").find('td input[type=checkbox]').eq(i).attr('name',name_temp+'_'+$i_pagos+'[]');
							});
							table.find("tr.tr_padre").find('input[type="checkbox"]').attr('checked',false);

						}

						//veremos si es la de condicion  para pasar los values a sus correspondientes caracteristicas
						if(table.attr('agregado') == 'condicion')
						{

							table.find("tr.tr_padre").find('td input').each(function(i)
							{
								valueselect = table.find("tr.tr_padre").find('td input').eq(i).val();
								//table.find("tr:last").find('td input').eq(i).val(valueselect);
								table.find("tr:last").find('td input').eq(i).val(valueselect);
							});
						}


						//AHORA EN ESTE EACH OBTENDREMOS LOS SELECTED DE LOS DISTRITOS
						array_texto_distrito = '';
						table.find("tr.tr_padre").find('td select#distrito').find('option').each(function(i)
						{
							if(jQuery(this).attr('selected') == 'selected'){
								array_texto_distrito+=jQuery(this).text()+',';
							}
						});
						array_texto_distrito = array_texto_distrito.substring(0,array_texto_distrito.length-1);
						
						//agregaremos un campo de texto oculto con la lista de los text de cada distrito
						if(table.attr('agregado') == 'general')
						{
							tempdistrito.after('<input type="hidden" name="distritos_text_'+valortabla+'[]" value="'+array_texto_distrito+'">');
							tempregion.after('<input type="hidden" name="regiones_text_'+valortabla+'[]" value="'+table.find("tr:last").find('td select#region option:selected').text()+'">');
							tempprovincia.after('<input type="hidden" name="prov_text_'+valortabla+'[]" value="'+table.find("tr:last").find('td select#provincia option:selected').text()+'">');
							tempdistrito.after('<input type="hidden" name="posicion_distritos_general[]" value="'+$i_general+'">');
						}
						if(table.attr('agregado') == 'pagos')
						{
							tempdistrito.after('<input type="hidden" name="distritos_text_'+valortabla+'[]" value="'+array_texto_distrito+'">');
							tempregion.after('<input type="hidden" name="regiones_text_'+valortabla+'[]" value="'+table.find("tr:last").find('td select#region option:selected').text()+'">');
							tempprovincia.after('<input type="hidden" name="prov_text_'+valortabla+'[]" value="'+table.find("tr:last").find('td select#provincia option:selected').text()+'">');
							tempdistrito.after('<input type="hidden" name="posicion_distritos_pagos[]" value="'+$i_pagos+'">');
						
						}
						if(table.attr('agregado') == 'condicion'){
							tempdistrito.after('<input type="hidden" name="distritos_text_'+valortabla+'[]" value="'+array_texto_distrito+'">');
							//guardamos la posicion del distritos
							tempdistrito.after('<input type="hidden" name="posicion_distritos_condicion[]" value="'+$i_condicion+'">');
							tempregion.after('<input type="hidden" name="regiones_text_'+valortabla+'[]" value="'+table.find("tr:last").find('td select#region option:selected').text()+'">');
							tempprovincia.after('<input type="hidden" name="prov_text_'+valortabla+'[]" value="'+table.find("tr:last").find('td select#provincia option:selected').text()+'">');		
						}

						//removeremos todo del tr padre
						//limpiaremos el body
						jQuery("tr.tr_padre").find('td ul.select2-selection__rendered li').remove();
						jQuery("tr.tr_padre").find('td input[type=text]').val("");
						jQuery("tr.tr_padre").find('td select option').removeAttr('selected');
						
						//removeremos la clase al final	
						if(table.attr('agregado') == 'condicion')
						{
							jQuery("tr.temp_class").find('td input.regiones_text_condicion').remove();
							jQuery("tr.temp_class").find('td input.regiones_condicion').remove();
							jQuery("tr.temp_class").find('td input.provincias_condicion').remove();
							jQuery("tr.temp_class").find('td input.prov_text_condicion').remove();
							jQuery("tr.temp_class").find('td input.distritos_text_condicion').remove();
							jQuery("tr.temp_class").find('td input.posicion_distritos_condicion').remove();
						}
						jQuery("tr.temp_class").removeAttr('class');
					});


					//----------------FUNCIONES DE COMBOS DINAMICOS PARA LOS DISTRITOS---------------------------
					//funcion para traernos las provincias por medio de la region
					jQuery(document).on('change','#region',function(){
						 select = jQuery(this);
						 var data = {
				            action: 'wooshc_get_province',
				            region_id: jQuery(this).val(),
				            wooshc_save_settings:wooshc_save_settings
				        };
				        jQuery.ajax({
				            type: 'POST',
				            url:'<?php echo admin_url("admin-ajax.php"); ?>',
				            data: data,
				            success: function(html){
				            	select.parent().parent().find('td select#provincia').html(html);
				            }
				        });
					});
					//funcion para seleccionar el distrito por medio de la provincia
					jQuery(document).on('change','#provincia',function(){
						 select = jQuery(this);
						// alert(jQuery(this).val());
						 var data = {
				            action: 'wooshc_get_district',
				            province_id: jQuery(this).val(),
				            wooshc_save_settings:wooshc_save_settings

				        };
				        jQuery.ajax({
				            type: 'POST',
				            url:'<?php echo admin_url("admin-ajax.php"); ?>',
				            data: data,
				            success: function(html){
				            	select.parent().parent().find('td select#distrito').html(html);
				            }
				        });

					});
					jQuery(document).on('change','#distrito',function(){
						//init_select_district();
					});

					jQuery(document).on('click','.nav-tab',function()
					{
						clase = $(this).attr('tab');
						//1  = tab_costo_por_distritos
						//2 = tab_configuracion_metodo_pago
						//3 = tab_condiciones_costo
						if(clase == 'tab_costo_por_distritos')
						{
							
							$("#mytable2_wrapper").hide(0);
							$("#mytable_condicion_wrapper").hide(0);
							$("#mytable_wrapper").show(0);

							//paginaciones
							$("#paginacion_tab1").show(0);
							$("#paginacion_tab2").hide(0);
							$("#paginacion_tab3").hide(0);
						}

						if(clase == 'tab_configuracion_metodo_pago')
						{
							
							$("#mytable2_wrapper").show(0);
							$("#mytable_condicion_wrapper").hide(0);
							$("#mytable_wrapper").hide(0);

							//paginaciones
							$("#paginacion_tab1").show(0);
							$("#paginacion_tab2").hide(0);
							$("#paginacion_tab3").hide(0);
						}


						if(clase == 'tab_condiciones_costo')
						{
							
							$("#mytable2_wrapper").hide(0);
							$("#mytable_condicion_wrapper").show(0);
							$("#mytable_wrapper").hide(0);

							//paginaciones
							$("#paginacion_tab1").hide(0);
							$("#paginacion_tab2").hide(0);
							$("#paginacion_tab3").show(0);
						}



						$(".tab-mamitayyo").hide(0);
						$("."+clase).fadeIn(200);
					});
				

					//$("#mytable2")
					
					/*$(".tr_padre").each(function(i){
						//
						tabla = $(this).parent().parent().attr("id");
						//alert(tabla);
						//mover a la primera posicion del tbody
						$(this).prependTo($("#"+tabla).find('tbody'));

					});*/
				


				});
			</script>
			<style type="text/css">
				tr.tr_padre
				{
					background-color: #ccc;
					padding:10px;
				}
			</style>
	