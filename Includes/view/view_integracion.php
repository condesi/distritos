<?php

	$shipping_integracion = get_option('shipping_integracion',true);
	$shipping_integracion = is_array($shipping_integracion) ? $shipping_integracion : array();
?>


<h1>Integraciones Shipping Calculate</h1>
<hr>

<form action="<?php echo admin_url('admin-post.php'); ?>" method="POST"> 
<input type="hidden" name="action" value="shipping_integracion_action">
<table id="customers">
  <tr>
    <th>--</th>

    <th>Plugin</th>
    <th>Descripcion</th>
  </tr>
  <tr>
  	<td>
    	<input  <?php  if (in_array("shipping_costo_minimo", $shipping_integracion)) 
    	{ echo 'checked'; } ?> type="checkbox" name="shipping_integracion[]" value="shipping_costo_minimo">
    </td>
    <td><a href="<?php echo get_home_url(); ?>/wp-admin/admin.php?page=wc-settings&tab=settings_pedido_minimo_tab">Shipping Costo Minimo</a></td>
    <td>
    	<p>Nos permite minimizar la cantidad  de costo minimo en un carro de compras, o total en la orden</p>
    </td>
  </tr>


   <tr>
  	<td>
    	<input  <?php  if (in_array("shipping_qr", $shipping_integracion)) 
    	{ echo 'checked'; } ?> type="checkbox" name="shipping_integracion[]" value="shipping_qr">
    </td> 
    <td><a href="<?php echo get_home_url(); ?>/wp-admin/admin.php?page=wc-settings&tab=checkout">Shipping qr</a></td>
    <td>
    	<p>Nos permite añadir codigo qr en yape como metodo de pago en woocommerce</p>
    </td>
  </tr>

 <tr>
    <td>
      <input  <?php  if (in_array("shipping_marca_descuentos", $shipping_integracion)) 
      { echo 'checked'; } ?> type="checkbox" name="shipping_integracion[]" value="shipping_marca_descuentos">
    </td> 
    <td><a href="<?php echo get_home_url(); ?>/wp-admin/admin.php?page=marca_descuentos">Shipping Descuentos por marcas</a></td>
    <td>
      <p>Nos permite realizar descuentos de productos en su precio por marcas</p>
    </td>
  </tr>

  <tr>
    <td>
      <input  <?php  if (in_array("shipping_whatsapp", $shipping_integracion)) 
      { echo 'checked'; } ?> type="checkbox" name="shipping_integracion[]" value="shipping_whatsapp">
    </td> 
    <td><a href="<?php echo get_home_url(); ?>/wp-admin/admin.php?page=nta_whatsapp">Shipping Whatsapp</a></td>
    <td>
      <p>Duplica el boton de whatsapp en woocommerce</p>
    </td>
  </tr>

   <tr>
    <td>
      <input  <?php  if (in_array("shipping_configuracion_ciudades", $shipping_integracion)) 
      { echo 'checked'; } ?> type="checkbox" name="shipping_integracion[]" value="shipping_configuracion_ciudades">
    </td> 
    <td><a href="<?php echo get_home_url(); ?>/wp-admin/admin.php?page=nta_whatsapp">Shipping Configuracion de Departamentos</a></td>
    <td>
      <p>Permite definir cuales departamenos seran visibles en los selectbox para envios por distrito</p>
    </td>
  </tr>

</table>

<br>
<button class="button button-primary">Guardar</button>
</form>




<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #CC99C2;
  color: white;
}
</style>