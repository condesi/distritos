<?php
        //comienza en vacio
        WC()->session->set( 'radio_chosen','');
        $user_id = get_current_user_id();
      

    ?>
    


    <style type="text/css">
            /*radio botones cheackout*/
        label.radio{
            display:block !important;
            margin-left:20px !important;
            position:relative;
            bottom:30px;
            margin-bottom:0 !important;
            margin-top:20px;
        }

         input[name='radio_choice']:after {
                width: 13px;
                height: 13px;
                        !important;
                border-radius: 13px;
                top: -2px;
                left: 0px;
                position: relative;
                background-color: #fff;
                content: '';
                display: inline-block;
                visibility: visible;
                border: 1px solid #222;
            }

            input[name='radio_choice']:checked:after {
                width: 13px;
                height: 13px;
                border-radius: 13px;
                top: -2px;
                left: -1px;
                position: relative;
                background-color: #666;
                content: '';
                display: inline-block;
                visibility: visible;
                border: 1px solid #222;
            }

        </style>


    <p class="form-row form-row-wide validate-required validate-district" id="billing_district_field" data-priority="110">
        <label for="billing_district" class="">Departamento(Region) <abbr class="required" title="obligatorio">*</abbr></label>
        <?php 
            $regions = wooshc_get_regions();
        ?>
        <select id="region" name="region" class="input-text">
             <option value="">Seleccione la region</option>
                  <?php 
                    foreach ($regions as $data) {
                    ?>
                    <option <?php selected($regi,$data->id); ?> value="<?php echo $data->id; ?>"><?php echo $data->name; ?></option>
                <?php  } ?>
        </select>
    </p>

    <p class="form-row form-row-wide validate-required validate-district" id="billing_district_field" data-priority="110">
        <label for="billing_district" class="">Provincia <abbr class="required" title="obligatorio">*</abbr></label>
        <select id="provincia" class="input-text" name="provincia">
             <option value="">Seleccione la provincia</option>
         </select>    
    </p>

    <p class="form-row form-row-wide validate-required validate-district" id="billing_district_field" data-priority="110">
        <label for="billing_district" class="">Distrito <abbr class="required" title="obligatorio">*</abbr></label>
        <select id="distritos" name="distrito" class="input-text">
             <option value="">Seleccione el Distrito</option>
         </select> 
         <div id="mensaje" style="display:none;"></div>   
    </p>






<style type="text/css">
    select.input-text,input[type=date].input-text{
        background: #FAFAFA !important;
        color: #666 !important;
        border: 1px solid #ccc !important;
        border-radius: 0 !important;
        padding: 10px 15px !important;
        -moz-box-sizing: border-box !important;
        -webkit-box-sizing: border-box !important;
        box-sizing: border-box !important;
        max-width: 100% !important;
    }
    #billing_city_field,#billing_state_field{
       display: none !important;
    }
    tr.shipping{
        display: none !important;
    }

    .select_metodos span input[type='radio']
    {
        float: left;
    }
    .select_metodos span label
    {
        width: 90%;
    }


</style>
<script type="text/javascript">
jQuery(document).ready(function () {
            var init = 0;
	//  jQuery("#container-data-radio").insertBefore('#order_payment_heading');
             jQuery("#container-data-radio").insertBefore('#checkout-radio');
             jQuery("#billing_address_1").attr('maxlength',100);
             jQuery("#billing_address_2").attr('maxlength',99);

            jQuery(document).on('change','#distritos',function () {
                if(jQuery(this).val()!='')
                {
                    if(jQuery("#ship-to-different-address-checkbox").is(':checked') !== true){
                        get_ajax_costes();
                    }

                }
            });
            function get_ajax_costes()
            {
                jQuery("#container-data-radio").html("<p>Cargando metodos de envio.....</p>");
                var billing_district = jQuery('#distritos').val();
                var billing_regions = jQuery('#region').val();
                var billing_province = jQuery('#provincia').val();
                var distrito_texto = jQuery('#distritos option:selected').text();

                //Aqui eligiremos el distrito 
                jQuery("#billing_city").val(distrito_texto);

                console.log(billing_district);
                jQuery("#mensaje").text('Cargando costos de envio....').show(200);
                var data = {
                    action: 'woocommerce_apply_district',
                    district: billing_district,
                    region:billing_regions,
                    province:billing_province
                };

                jQuery.ajax({
                    type: 'POST',
                    url:'<?php echo admin_url("admin-ajax.php"); ?>',
                    data: data,
                    success: function (code) {
                        if (code === '0') {
                            //jQuery("#mensaje").text('Cargando costo de envio....').hide(0);
                             jQuery('body').trigger('update_checkout');
                             jQuery("#mensaje").text("");
                             jQuery("#container-data-radio").html("");
                            
                        }else{
                            console.log(code);
                            console.log("Cambio");
                            //console.log(code);
                            jQuery("#container-data-radio").html(code);
                            jQuery("#mensaje").text("");

                            //vamos a recorrer los elementos dentro del each 
                            jQuery("input[name='radio_choice']").each(function()
                            {
                                if(jQuery(this).val() == '-Regular')
                                {
                                    jQuery(this).hide(0);
                                    jQuery(this).next('label').remove();
                                }
                            });


                        }
                    },
                    dataType: 'html'
                });
                return false;
            }
            //funcion para traernos las provincias por medio de la region
            jQuery(document).on('change','#region',function(){
                get_ajax_province();
            });
            get_ajax_province();
            function get_ajax_province()
            {
                select = jQuery('#region');
               // jQuery("select[name='billing_state']").val(select.val());
                jQuery("select[name='billing_state']").val(select.val()).trigger('change');
                jQuery("input[name='billing_state']").val(select.val());


                selected = getCleanedString(jQuery('#region option:selected').text());
                //colocaremos la region por defecto
                jQuery("#billing_state option").removeAttr('selected');
                jQuery("#billing_state option").each(function(){
                     text = getCleanedString(jQuery(this).text());
                     if(text.indexOf(selected) >(-1)){
                        jQuery('#select2-billing_state-container').text(text);
                        jQuery('#select2-billing_state-container').attr('title',text);
                        jQuery(this).attr('selected',true);
                        return false;
                     }
                });
                //Esto pasara antes de hacer el ajax
                jQuery('.msg_provincia').remove();
                 jQuery('select#provincia').text('');
                 jQuery('select#provincia').before('<div class="msg_provincia"><b>Cargando Provincias...</b></div>');
                 var data = {
                    action: 'wooshc_get_province',
                    region_id: jQuery('#region').val()
                };
                jQuery.ajax({
                    type: 'POST',
                    url:'<?php echo admin_url("admin-ajax.php"); ?>',
                    data: data,
                    success: function(html){
                        jQuery('select#provincia').html(html);
                        jQuery('.msg_provincia').remove();
                            get_ajax_distritos();
                    }
                });


                  select = jQuery('#region');
               // jQuery("select[name='billing_state']").val(select.val());
                jQuery("select[name='billing_state']").val(select.val()).trigger('change');


            }
            //funcion para seleccionar el distrito por medio de la provincia
            jQuery(document).on('change','#provincia',function(){
              get_ajax_distritos();
            });
            function get_ajax_distritos(){
                 jQuery('.msg_distritos').remove();
                 jQuery('select#distritos').text('');
                 jQuery('select#distritos').before('<div class="msg_distritos"><b>Cargando Distritos...</b></div>');
                 var data = {
                    action: 'wooshc_get_district',
                    province_id: jQuery('#provincia').val(),
                    region_id:jQuery('#region').val()
                };

                jQuery.ajax({
                    type: 'POST',
                    url:'<?php echo admin_url("admin-ajax.php"); ?>',
                    data: data,
                    success: function(html){
                        jQuery('select#distritos').html(html);
                        jQuery('.msg_distritos').remove();
                        <?php if(is_object($checkout)) echo "get_ajax_costes();"; ?>
                    }
                });
                 
            }
            //---
            function getCleanedString(cadena){
               // Definimos los caracteres que queremos eliminar
               var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?,.";
               // Los eliminamos todos
               for (var i = 0; i < specialChars.length; i++) {
                   cadena= cadena.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
               }   
               // Lo queremos devolver limpio en minusculas
               cadena = cadena.toLowerCase();
               // Quitamos espacios y los sustituimos por _ porque nos gusta mas asi
              // cadena = cadena.replace(/ /g,"_");}
               // Quitamos acentos y "ñ". Fijate en que va sin comillas el primer parametro
               cadena = cadena.replace(/á/gi,"a");
               cadena = cadena.replace(/é/gi,"e");
               cadena = cadena.replace(/í/gi,"i");
               cadena = cadena.replace(/ó/gi,"o");
               cadena = cadena.replace(/ú/gi,"u");
               cadena = cadena.replace(/ñ/gi,"n");
               return cadena;
            }
           
            //aqui haremos un calculo de los costos segun el distrito
            jQuery("#ship-to-different-address-checkbox").change(function() {
               if(jQuery(this).is(':checked')==false){
                    jQuery("#action_form").val('billing');
                    get_ajax_costes();
                }else{
                    jQuery("#action_form").val('shipping');    
                }
            });

         
            function get_data_init()
            {

                element_data = setInterval(function()
                {
                select = jQuery('#region');
                  if(select.val() != ''){
                        // jQuery("select[name='billing_state']").val(select.val());
                        jQuery("select[name='billing_state']").val(select.val()).trigger('change');
                        clearInterval(element_data);
                 }
                },500);

            }
            get_data_init();



            

           

});

</script>





