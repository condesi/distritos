<?php

	add_action('admin_menu', 'wooshc_submenu_settings');
	function wooshc_submenu_settings()
	{

		 add_menu_page(
        __( 'shipping_calculate', '' ),
        'Shipping Calculate',
        'manage_options',
        'fn_shipping_calculate',
        'wooshc_template_setting_fn',
        '',
        6
    	);

		//Agregamos menu de configuracion
		add_submenu_page(
			'fn_shipping_calculate',          // el slug en donde mostraremos el submenu

			__( 'Costos por Distritos', '' ), // titulo de la pagina

		    __( 'Costos por Distritos', '' ), //  titulo del menu

				'manage_options',               // capacidad requerida para ver esta pagina 

				'fn_shipping_calculate',                //  nombre de la pagina o su slug, e.g. options-general.php?page=wooshc_template_setting

				'wooshc_template_setting_fn'           // funcion callback en donde  colocaremos o que va dentro de la pagina
		);


		add_submenu_page('fn_shipping_calculate', 
		   	'configuracion_horario', 
		   	'configuracion de horario', 
		    'manage_options', 
		   	'mmyo_settings', 
		   	'mmyo_settings_page', 
		   	'dashicons-schedule',
		   	 2 );


		add_submenu_page(
			'fn_shipping_calculate',          // el slug en donde mostraremos el submenu

			__( 'Integraciones', '' ), // titulo de la pagina

		    __( 'Integraciones', '' ), //  titulo del menu

				'manage_options',               // capacidad requerida para ver esta pagina 

				'fn_shipping_integraciones',                //  nombre de la pagina o su slug, e.g. options-general.php?page=wooshc_template_setting

				'fn_shipping_integraciones_page',           // funcion callback en donde  colocaremos o que va dentro de la pagina
				
   	 			3
		);
    
	}


    function fn_shipping_integraciones_page(){

		require_once wooshc_path.'includes/view/view_integracion.php';
    }

    add_action('admin_post_shipping_integracion_action','shipping_integracion_action_callback');
    function shipping_integracion_action_callback()
    {
    	update_option('shipping_integracion',$_POST['shipping_integracion']);
   		wp_redirect(wp_get_referer());
    }




	function wooshc_template_setting_fn()
	{
		require_once wooshc_path.'includes/view/view_settings.php';
	}
	//funciones ajax para traernos el select de provincias

	add_action('wp_ajax_wooshc_get_province','wooshc_get_province');
	add_action('wp_ajax_nopriv_wooshc_get_province','wooshc_get_province');
	function wooshc_get_province()
	{
		$region_id = $_POST['region_id'];
		$provinces = wooshc_get_provinces_regions($region_id);
   		//provincia del usuario si tiene seleccionado
   		$prov = get_user_meta(get_current_user_id(),'wooshc_provincia',true);
		$html = '';
		$html.='<option  value="">Seleccione la provincia</option>';
		foreach ($provinces as $data) {	
			
			if(!isset($_POST['wooshc_save_settings']))
			{
				$html.='<option '.selected($prov,$data->id).' value="'.$data->id.'">'.$data->name.'</option>';
			}else{
				$html.='<option  value="'.$data->id.'">'.$data->name.'</option>';
			}
		} 
		echo $html;
		wp_die();
	}

	//funciones ajax para traernos el select de districtos
	add_action('wp_ajax_wooshc_get_district','wooshc_get_district');
	add_action('wp_ajax_nopriv_wooshc_get_district','wooshc_get_district');
	function wooshc_get_district()
	{
		$province_id = $_POST['province_id'];
		$region_id = $_POST['region_id'];
		$districts = wooshc_get_districts_province($province_id,$region_id);
   		$dist = get_user_meta(get_current_user_id(),'wooshc_distrito',true);
		$html = '';
		$html.='<option value="">Seleccione el distrito</option>';
		foreach ($districts as $data) {
			if(!isset($_POST['wooshc_save_settings'])){			
				$html.='<option '.selected($dist,$data->id).' value="'.$data->id.'">'.$data->name.'</option>';
			}else{
				$html.='<option value="'.$data->id.'">'.$data->name.'</option>';
			}
		} 
		echo $html;
		wp_die();
	}
	//funcion post para guardar los datos del settings
	add_action('admin_post_wooshc_save_settings','save_settings');
	add_action('admin_post_nopriv_wooshc_save_settings','save_settings');
	function save_settings()
	{


		//AQUI GUARDAREMOS LOS DATOS PRINCIPALES DE LOS PREICOS POR DISTRITOS O COSTOS POR DISTRITOS GLOBALES
		delete_option('wooshc_option',true);
		unset($_POST['precios'][0]);
		foreach ($_POST['precios'] as $key => $value) {
			//$pos = $_POST['precios'][$key];	

			$pos_distrito = $_POST['posicion_distritos_general'][$key];
			$data[$key] = array(
				'regiones' => $_POST['regiones_general'][$key],
				'regiones_text'=>$_POST['regiones_text_general'][$key],
				'provincias' => $_POST['provincias_general'][$key],
				'prov_text' => $_POST['prov_text_general'][$key],
				'distritos' => $_POST['distritos_general_'.$pos_distrito],
				'distritos_text' => $_POST['distritos_text_general'][$key],
				'precios' => $_POST['precios'][$key],
				'express' => $_POST['express'][$key],
				'programado' => $_POST['programado'][$key],
			);
		
		}	
		update_option('wooshc_option',$data);
		file_put_contents(wooshc_path.'includes/wooshc_option.txt', serialize($data));



		//----------------AQUI GUARDAREMOS LOS PRECIOS POR  CONDICION--------------------------
		delete_option('wooshc_option_pagos',true);
		$data_pagos = array();
		unset($_POST['posicion_distritos_pagos'][0]);
		$i = 0;
		foreach ($_POST['posicion_distritos_pagos'] as $key => $value) {
			//$pos = $_POST['precios'][$key];
			$i++;
			$pos_distrito = $_POST['posicion_distritos_pagos'][$i];
			$data_pagos[$key] = array(
				'regiones' => $_POST['regiones_pagos'][$i],
				'regiones_text'=>$_POST['regiones_text_pagos'][$i],
				'provincias' => $_POST['provincias_pagos'][$i],
				'prov_text' => $_POST['prov_text_pagos'][$i],
				'distritos' => $_POST['distritos_pagos_'.$pos_distrito],
				'distritos_text' => $_POST['distritos_text_pagos'][$i],
				'gateways' => $_POST['wp_payment_'.$pos_distrito]
			);
			
		}	
		update_option('wooshc_option_pagos',$data_pagos);
		file_put_contents(wooshc_path.'includes/wooshc_option_pagos.txt', serialize($data_pagos));



		///------------VAMOS A RECORRER LA PARTE DE CONDICIONES---------
		$data_condition = array();
		//removemos todo el arreglo primeramente
		delete_option('wooshc_condition',true);
		//Guardar condiciones de enviy
		unset($_POST['precios_condicion'][0]);

		print_r($_POST['posicion_distritos_condicion']);
		$data_condition = array();
		foreach ($_POST['precios_condicion'] as $key => $value) {
			//$pos = $_POST['precios_condicion'][$key];-
			//$data_condition[$pos] = array();
			//con esto obtenemos el valor del distrito
			echo $key;
			echo '<br>';

			$pos_distrito = $_POST['posicion_distritos_condicion'][$key];
			//
			echo " el post distrito ".$pos_distrito;
			echo "<br> EXPRESS.. ".$_POST['mayor_menor_costo_'.$pos_distrito];
			echo "RESULTASDO COSTO ".$_POST['resultado_costo_'.$pos_distrito];
			//echo $pos_distrito;
			//print_r($_POST['distritos_condicion_'.$pos_distrito]);

			echo '<hr>';
			$data_condition[$key] = array(
				'mayor_menor_costo'=>$_POST['mayor_menor_costo_'.$pos_distrito],
				'resultado_costo'=>$_POST['resultado_costo_'.$pos_distrito],
				'y_o'=>$_POST['y_o_'.$pos_distrito],
				'mayor_menor_peso'=>$_POST['mayor_menor_peso_'.$pos_distrito],
				'resultado_peso'=>$_POST['resultado_peso_'.$pos_distrito],
				//'resultado_costo_envio'=>$_POST['resultado_costo_envio'][$key],
				'distritos' => $_POST['distritos_condicion_'.$pos_distrito],
				'regiones' => $_POST['regiones_condicion'][$key],
				'regiones_text'=>$_POST['regiones_text_condicion'][$key],
				'provincias' => $_POST['provincias_condicion'][$key],
				'prov_text' => $_POST['prov_text_condicion'][$key],
				'distritos_text' => $_POST['distritos_text_condicion'][$key],
				'precios' => $_POST['precios_condicion'][$key],
				'express' => $_POST['express_condicion'][$key],
				'programado' => $_POST['programado_condicion'][$key],
			);

		}


		//guardareos los datos de condiciones
		update_option('wooshc_condition',$data_condition);
		file_put_contents(wooshc_path.'includes/wooshc_condition.txt', serialize($data_condition));


		//guardar mensaje de llamada en el front end cuando un distrito no esta validado
		update_option('distric_no_configure',$_POST['distric_no_configure']);
		//guardar los gateways por default
		update_option('wooshc_gateways_default',$_POST['wp_payment_default']);
		wp_redirect(wp_get_referer());

	}
