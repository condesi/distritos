<?php

/*

	Plugin Name: Woocommerce Shipping Calculate

	Plugin URI: 

	Description: Calcular Costo de envio por distritos

	Version: 2.0

	Author: Alberto Vargas

	Author URI: https://www.condesi.pe

	License:

*/



define('wooshc_path',plugin_dir_path(__FILE__));

define('wooshc_url',plugin_dir_url(__FILE__));



require_once wooshc_path.'functions.php';

//require_once wooshc_path.'functions.php';
require_once wooshc_path.'includes/action_filter.php';
require_once wooshc_path.'includes/settings.php';

//Añadiremos snippet  aqui en shipping calculate que sirva como codigo que podamos usar globalmente
require_once wooshc_path.'snippet-code.php';

//INTEGRACIONES
require_once wooshc_path.'integraciones/wc-pedido-minimo/init.php';
require_once wooshc_path.'wp_mamitayyo_horarios/wp_mamitayyo_horarios.php';
require_once wooshc_path.'integraciones/qr-shipping/plugis11/mc-quetma.php';
require_once wooshc_path.'integraciones/shipping-marcas-descuento/shipping-marcas-descuento.php';
require_once wooshc_path.'integraciones/shipping_whatsapp/whatsapp.php';
require_once wooshc_path.'integraciones/shipping-configuracion-ciudades/shipping-configuracion-ciudades.php';





//Agregar campos personalizados en la cuenta del usuario
add_action('woocommerce_edit_account_form','action_woocommerce_edit_account');
function action_woocommerce_edit_account()
{
    $user_id = get_current_user_id();
    require_once wooshc_path.'includes/view/view_custom_edit_account.php';

}




//AGREGAR CAMPS NUEVOS EN LA SECCION DE DIRECCION DE ENVIO EN EL CHECKOUT Y EN EL ADMIN DEL USUARIO 
add_action( 'woocommerce_after_checkout_billing_form', 'action_woocommerce_after_checkout_form', 10, 1 ); 
add_action('woocommerce_after_edit_address_form_billing','action_woocommerce_after_checkout_form');
add_action('woocommerce_after_edit_address_form_shipping','action_woocommerce_after_checkout_form');
function action_woocommerce_after_checkout_form( $checkout ) { 
    //removemos el shipping para que no quede rastros
    @session_start();

    global $woocommerce;

    global $load_address;

    global $post;

    $_SESSION['val'] = 0;

    $_SESSION['gate'] = 'defaults';



    //datos de region / provincia / distrito por usuarios

    $regi = get_user_meta(get_current_user_id(),'wooshc_region',true);

    //llamamos a la vista  del custon form checkout

    require_once wooshc_path.'includes/view/view_custom_form_checkout_1.php';

}; 





//--------------------ESTO ES SOLO PARA LA PARTE DE DIRECCION DIFERENTE---------------------

add_action('woocommerce_after_checkout_shipping_form','shipping_diferent_account_p');
function shipping_diferent_account_p()
{

    //removemos el shipping para que no quede rastros
    global $woocommerce;
    global $load_address;
    @session_start();
    $_SESSION['val'] = 0;
    $_SESSION['gate'] = 'defaults';
    WC()->cart->add_fee('Shipping ',0);
    $amount = preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_total());
    $amount = floatval(substr($amount,2));
    $total_items = floatval(WC()->cart->get_cart_contents_count());
    //datos de region / provincia / distrito por usuarios
    $regi = get_user_meta(get_current_user_id(),'wooshc_region',true);
    //llamamos a la vista  del custon form checkout
    require_once wooshc_path.'includes/view/view_custom_form_checkout_2.php';

}; 





//validar campos costumizados que sean obligatorios al comprar
add_action ('woocommerce_checkout_process', 'validar_campos_costumizados');
function validar_campos_costumizados()
{   
    if($_POST['action_form'] == 'billing'){    
     
        //VER SI EXISTEN LOS DISTRITOS
             $wooshc_option = get_option('wooshc_option',true);
             $exist_distrito = 0;
            $distrito_break = '';
            $distrito_break_opcion = '';

              //options principal para buscarl a lista de distritos
              foreach ($wooshc_option as $key => $value) 
              {

                  if($wooshc_option[$key]['regiones'] == $_POST['region'])
                  {
                       $val = $wooshc_option[$key]['precios'];
                       $exist_distrito = 1;
                       if(intval($_POST['province']) == 0 && intval($wooshc_option[$key]['provincias'])==0){
                         $distrito_break = $wooshc_option[$key]['precios'];
                      }
                  }

                  if(intval($wooshc_option[$key]['regiones']) > 0  && intval($wooshc_option[$key]['provincias']) >0  &&  intval($_POST['province'])>0 && intval($_POST['region'])>0)
                  {
                    if($wooshc_option[$key]['regiones'] == $_POST['region'] &&  $wooshc_option[$key]['provincias']==$_POST['province'])
                    {
                       $val = $wooshc_option[$key]['precios'];
                       $exist_distrito = 1;
                    }else{

                       foreach ($wooshc_option as $key_repe1 => $value_repe1) {
                           if($wooshc_option[$key]['regiones'] == $_POST['region'])
                          {
                               $val = $wooshc_option[$key_repe1]['precios'];
                               $exist_distrito = 1;
                               break;
                          }
                       }
                    }//repetiremos el foreach a ver si contiene la region para darsela al de la regon
                  }



                //buscamos los distritos dentro del arreglo distritos
                foreach ($wooshc_option[$key]['distritos'] as $key2 => $value2)
                 { 
                   if( ($wooshc_option[$key]['distritos'][$key2] == $_POST['district']) && ($wooshc_option[$key]['regiones']==$_POST['region']) && ($wooshc_option[$key]['provincias']==$_POST['province']) ) {
                      $exist_distrito = 1;
                      break;
                    }
                }//cierre foreach secundario
              }//cieerre foreach de wooshc_option


              $wooshc_condition = get_option('wooshc_condition',true);
              foreach ($wooshc_condition as $key => $value)
              {
                if($wooshc_option[$key]['regiones'] == $_POST['region'])
                  {
                       $val = $wooshc_option[$key]['precios'];
                       $exist_distrito = 1;
                       if(intval($_POST['province']) == 0 && intval($wooshc_option[$key]['provincias'])==0){
                         $distrito_break = $wooshc_option[$key]['precios'];
                      }
                  }

                  if(intval($wooshc_option[$key]['regiones']) > 0  && intval($wooshc_option[$key]['provincias']) >0  &&  intval($_POST['province'])>0 && intval($_POST['region'])>0)
                  {
                    if($wooshc_option[$key]['regiones'] == $_POST['region'] &&  $wooshc_option[$key]['provincias']==$_POST['province'])
                    {
                       $val = $wooshc_option[$key]['precios'];
                       $exist_distrito = 1;
                    }else{

                       foreach ($wooshc_option as $key_repe2 => $value_repe2) {
                           if($wooshc_option[$key]['regiones'] == $_POST['region'])
                          {
                               $val = $wooshc_option[$key_repe2]['precios'];
                               $exist_distrito = 1;
                               break;
                          }
                       }
                    }//repetiremos el foreach a ver si contiene la region para darsela al de la regon
                  }



                foreach ($wooshc_condition[$key]['distritos'] as $key2 => $value2) { 
                    
                    if( ($wooshc_condition[$key]['distritos'][$key2] == $_POST['district']) && ($wooshc_condition[$key]['regiones']==$_POST['region']) && ($wooshc_condition[$key]['provincias']==$_POST['province']) ) {


                      $exist_distrito = 1;
                      break;
                    }//cierre del if
                }//cierre del foreach secun dario
              }//cierre del foreach*/
             if($exist_distrito==0)
              {
                $distric_no_configure = get_option('distric_no_configure',true);

                $distric_no_configure = isset($distric_no_configure) ? $distric_no_configure : '';

                if($distric_no_configure == 1){

                  $distric_no_configure = '';

                }

                wc_add_notice (__($distric_no_configure), 'error');

              }

        }
    
    //---------------------CIERRE DEL BILLING

    if($_POST['action_form']=='shipping'){ 

   


        //VER SI EXISTEN LOS DISTRITOS

             $wooshc_option = get_option('wooshc_option',true);

             $exist_distrito = 0;

              //options principal para buscarl a lista de distritos

              foreach ($wooshc_option as $key => $value) {

                //buscamos los distritos dentro del arreglo distritos

                foreach ($wooshc_option[$key]['distritos'] as $key2 => $value2) { 

                    if( ($wooshc_option[$key]['distritos'][$key2] == $_POST['distrito2']) && ($wooshc_option[$key]['regiones'][0]==$_POST['region2']) && ($wooshc_option[$key]['provincias'][0]==$_POST['provincia2']) ) {

                      $exist_distrito = 1;

                      break;

                    }

                }//cierre foreach secundario

              }//cieerre foreach de wooshc_option

              $wooshc_condition = get_option('wooshc_condition',true);

              foreach ($wooshc_condition as $key => $value) {

                foreach ($wooshc_condition[$key]['distritos'] as $key2 => $value2) { 

                   if($wooshc_condition[$key]['distritos'][$key2] == $_POST['distrito2']){        

                      $exist_distrito = 1;

                      break;

                    }//cierre del if

                }//cierre del foreach secun dario

              }//cierre del foreach*/

              if($exist_distrito==0)

              {

                $distric_no_configure = get_option('distric_no_configure',true);

                $distric_no_configure = isset($distric_no_configure) ? $distric_no_configure : '';

                if($distric_no_configure == 1){

                  $distric_no_configure = '';

                }
                wc_add_notice (__($distric_no_configure), 'error');
              }

    }

}



//accion para guardar los datos de los campos nuevos agregados despues de la nota  esto es en el admin

add_action('woocommerce_checkout_update_order_meta', 'customise_checkout_field_update_order_meta');
function customise_checkout_field_update_order_meta($order_id)

{

    //vamos a obtener los datos del pedido

     if($_POST['action_form'] == 'billing'){  

    }
    //en el formulario de direccion diferente
    if($_POST['action_form']=='shipping'){ 
    }

}

//MOSTRAR LOS CAMPOS EN EL PANEL DE ADMINISTRACION
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );
function my_custom_checkout_field_display_admin_order_meta($order){

    global $post;


}



//CREAREMOS ELEMENTO PARA GUARDAR LOS DATOS DEL USUARIO

add_action('woocommerce_customer_save_address','wooshc_save_direction',10,2);

function wooshc_save_direction($user_id,$address)

{

    //guardar datos de la region / provincia / distrito

    update_user_meta($user_id,'wooshc_region',$_POST['region']);

    update_user_meta($user_id,'wooshc_provincia',$_POST['provincia']);

    update_user_meta($user_id,'wooshc_distrito',$_POST['distrito']);

}



function disable_shipping_calc_on_cart( $show_shipping ) {
    if( is_cart() ) {
        return false;
    }
    //session_start();
    //$show_shipping = $_SESSION['val'];
    
    return $show_shipping;
}
add_filter( 'woocommerce_cart_ready_to_calc_shipping', 'disable_shipping_calc_on_cart', 99 );

//remover el calculate
add_action('woocommerce_order_details_after_order_table_items','woosc_remove_shipping_order_table');
function woosc_remove_shipping_order_table($order)
{
  echo '<style>
      tfoot tr:nth-child(2){
        /*display:none;*/
      }
  </style>';
}


add_action('admin_head','function_script_temp');
function function_script_temp()

{

?>



<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css"/>



<?php

}

function custom_change_flat_rates_cost( $rates, $package ) {
	@session_start();
    // Make sure flat rate is available && custom val shipping by district
    if( isset($_SESSION['val']) && isset( $rates['flat_rate:8'] ) ){
        $customshipcost = $_SESSION['val'];
        $rates['flat_rate:8']->cost = $customshipcost;
    }
	return $rates;
}
