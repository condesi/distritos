<?php







	//Aqui haremos todas las consultas



	function wooshc_get_regions()

	{

		global $wpdb, $user_email, $user_login;
		$table_name = "wooshc_regions";
		$qry = $wpdb->get_results("SELECT * FROM $table_name");

		//vamos a manipular las regiones 
		$qry = apply_filters('before_return_wooshc_regions',$qry);

		return $qry;
	}

	//traer todas las provincias
	function wooshc_get_provinces()
	{
		global $wpdb, $user_email, $user_login;
		$table_name = "wooshc_provinces";
		$qry = $wpdb->get_results("SELECT * FROM $table_name");
		return $qry;
	}
	//traer todos los distritos

	function wooshc_get_districts()
	{
		global $wpdb, $user_email, $user_login;
		$table_name = "wooshc_districts";
		$qry = $wpdb->get_results("SELECT * FROM $table_name");
		return $qry;
	}

	//funcion para traernos una lista de provincias de una region
	function wooshc_get_provinces_regions($region_id)
	{
		global $wpdb, $user_email, $user_login;

		$table_name = "wooshc_provinces";

		if($region_id == null || $region_id == '') $region_id = 0;


		$qry = $wpdb->get_results("SELECT * FROM $table_name WHERE region_id=$region_id");

		return $qry;

	}



	//funcion para traernos todos los distritos de una provincia

	function wooshc_get_districts_province($province_id,$region_id)

	{

		if($province_id == null  || $province_id == '') $province_id = 0;

		global $wpdb, $user_email, $user_login;

		$table_name = "wooshc_districts";

		$qry = $wpdb->get_results("SELECT * FROM $table_name WHERE region_id=$province_id");

		return $qry;

	}
