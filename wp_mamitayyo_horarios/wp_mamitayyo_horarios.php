<?php 


require_once plugin_dir_path( __FILE__).'actions_filters.php';

/*Creacion de la vista del settings*/
function mmyo_settings_page()
{
	$mmyo_settings = get_option('mmyo_settings',true);
	$array_horarios = array('00:00AM','01:00AM','02:00AM','03:00AM','04:00AM','05:00AM','06:00AM','07:00AM','08:00AM','09:00AM','10:00AM','11:00AM','12:00PM','01:00PM','02:00PM','03:00PM','04:00PM','05:00PM','06:00PM','07:00PM','08:00PM','09:00PM','10:00PM','11:00PM');

	//vamos a colocar los array formateados
	$mmyo_settings['horario_programado_desde']['lunes'] = isset($mmyo_settings['horario_programado_desde']['lunes']) ? $mmyo_settings['horario_programado_desde']['lunes'] : array();
	
	$mmyo_settings['horario_programado_desde']['martes'] = isset($mmyo_settings['horario_programado_desde']['martes']) ? $mmyo_settings['horario_programado_desde']['martes'] : array();
	
	$mmyo_settings['horario_programado_desde']['miercoles'] = isset($mmyo_settings['horario_programado_desde']['miercoles']) ? $mmyo_settings['horario_programado_desde']['miercoles'] : array();
	
	$mmyo_settings['horario_programado_desde']['jueves'] = isset($mmyo_settings['horario_programado_desde']['jueves']) ? $mmyo_settings['horario_programado_desde']['jueves'] : array();
	
	$mmyo_settings['horario_programado_desde']['viernes'] = isset($mmyo_settings['horario_programado_desde']['viernes']) ? $mmyo_settings['horario_programado_desde']['viernes'] : array();
	
	$mmyo_settings['horario_programado_desde']['sabado'] = isset($mmyo_settings['horario_programado_desde']['sabado']) ? $mmyo_settings['horario_programado_desde']['sabado'] : array();
	
	$mmyo_settings['horario_programado_desde']['domingo'] = isset($mmyo_settings['horario_programado_desde']['domingo']) ? $mmyo_settings['horario_programado_desde']['domingo'] : array();



	//esto es para recojo en almacen
	$mmyo_settings['recojo_almacen_desde']['lunes'] = isset($mmyo_settings['recojo_almacen_desde']['lunes']) ? $mmyo_settings['recojo_almacen_desde']['lunes'] : array();
	
	$mmyo_settings['recojo_almacen_desde']['martes'] = isset($mmyo_settings['recojo_almacen_desde']['martes']) ? $mmyo_settings['recojo_almacen_desde']['martes'] : array();
	
	$mmyo_settings['recojo_almacen_desde']['miercoles'] = isset($mmyo_settings['recojo_almacen_desde']['miercoles']) ? $mmyo_settings['recojo_almacen_desde']['miercoles'] : array();
	
	$mmyo_settings['recojo_almacen_desde']['jueves'] = isset($mmyo_settings['recojo_almacen_desde']['jueves']) ? $mmyo_settings['recojo_almacen_desde']['jueves'] : array();
	
	$mmyo_settings['recojo_almacen_desde']['viernes'] = isset($mmyo_settings['recojo_almacen_desde']['viernes']) ? $mmyo_settings['recojo_almacen_desde']['viernes'] : array();
	
	$mmyo_settings['recojo_almacen_desde']['sabado'] = isset($mmyo_settings['recojo_almacen_desde']['sabado']) ? $mmyo_settings['recojo_almacen_desde']['sabado'] : array();
	
	$mmyo_settings['recojo_almacen_desde']['domingo'] = isset($mmyo_settings['recojo_almacen_desde']['domingo']) ? $mmyo_settings['recojo_almacen_desde']['domingo'] : array();


?>	
	<style type="text/css">
		table {
		  border-collapse: collapse;
		  width: 100%;
		}

		th, td {
		  text-align: left;
		  padding: 8px;
		}

		tr:nth-child(even){background-color: #f2f2f2}

		th {
		  background-color: #4CAF50;
		  color: white;
		}
		.hijo{
			margin-top:10px;
		}
	</style>

	<h1>Configuración Horarios</h1>
	<br>
	<form action="<?php echo admin_url('admin-post.php'); ?>" method="POST">
		<input type="hidden" name="action" value="mmyo_settings_action">	
		<h3>Regular</h3>
		
		<h5>Mensaje</h5>
		<p>Añade el mensaje a mostrar en el metodo de envio regular</p>
		<input type="text" name="mmyo_mensaje_regular" style="width: 90%; padding: 10px;" value="<?php echo @$mmyo_settings['mmyo_mensaje_regular']; ?>">

		<h5>Horarios</h5>
		<table width="100%">
			<tr>
				<td>Lunes</td>
				<td>Martes</td>
				<td>Miercoles</td>
				<td>Jueves</td>
				<td>Viernes</td>
				<td>Sabado</td>
				<td>Domingo</td>
			</tr>
			<tr>
				<td><input type="checkbox" <?php checked($mmyo_settings['horario_regular']['lunes'],'yes'); ?> name="horario_regular[lunes]" value="yes"></td>
				<td><input type="checkbox"  <?php checked($mmyo_settings['horario_regular']['martes'],'yes'); ?> name="horario_regular[martes]"  value="yes"></td>
				<td><input type="checkbox"  <?php checked($mmyo_settings['horario_regular']['miercoles'],'yes'); ?> name="horario_regular[miercoles]"  value="yes"></td>
				<td><input type="checkbox"  <?php checked($mmyo_settings['horario_regular']['jueves'],'yes'); ?> name="horario_regular[jueves]"  value="yes"></td>
				<td><input type="checkbox"  <?php checked($mmyo_settings['horario_regular']['viernes'],'yes'); ?> name="horario_regular[viernes]"  value="yes"></td>
				<td><input type="checkbox"  <?php checked($mmyo_settings['horario_regular']['sabado'],'yes'); ?> name="horario_regular[sabado]"  value="yes"></td>
				<td><input type="checkbox"  <?php checked($mmyo_settings['horario_regular']['domingo'],'yes'); ?> name="horario_regular[domingo]"  value="yes"></td>
			</tr>
		</table>
		<hr>

		<h3>Express</h3>
		<h5>Mensaje</h5>
		<p>Añade el mensaje a mostrar en el metodo de envio regular</p>
		<input type="text" name="mmyo_mensaje_express" style="width: 90%; padding: 10px;" value="<?php echo @$mmyo_settings['mmyo_mensaje_express']; ?>">

		<h5>Horarios</h5>
		<table width="100%">
			<tr>
				<td>Lunes</td>
				<td>Martes</td>
				<td>Miercoles</td>
				<td>Jueves</td>
				<td>Viernes</td>
				<td>Sabado</td>
				<td>Domingo</td>
			</tr>
				<tr>
				<td><input type="checkbox" <?php checked($mmyo_settings['horario_express']['lunes'],'yes'); ?> name="horario_express[lunes]" value="yes"></td>
				<td><input type="checkbox"  <?php checked($mmyo_settings['horario_express']['martes'],'yes'); ?> name="horario_express[martes]"  value="yes"></td>
				<td><input type="checkbox"  <?php checked($mmyo_settings['horario_express']['miercoles'],'yes'); ?> name="horario_express[miercoles]"  value="yes"></td>
				<td><input type="checkbox"  <?php checked($mmyo_settings['horario_express']['jueves'],'yes'); ?> name="horario_express[jueves]"  value="yes"></td>
				<td><input type="checkbox"  <?php checked($mmyo_settings['horario_express']['viernes'],'yes'); ?> name="horario_express[viernes]"  value="yes"></td>
				<td><input type="checkbox"  <?php checked($mmyo_settings['horario_express']['sabado'],'yes'); ?> name="horario_express[sabado]"  value="yes"></td>
				<td><input type="checkbox"  <?php checked($mmyo_settings['horario_express']['domingo'],'yes'); ?> name="horario_express[domingo]"  value="yes"></td>
			</tr>
		</table>
		<hr>
		
		<table width="70%" border="1" cellpadding="0" cellspacing="0">

			<h3>Programado</h3>
			<h5>Mensaje</h5>
			<p>Añade el mensaje a mostrar en el metodo de envio regular</p>
			<input type="text" name="mmyo_mensaje_programado" style="width: 90%; padding: 10px;" value="<?php echo @$mmyo_settings['mmyo_mensaje_programado']; ?>">

			<h5>Horarios</h5>
			<tr>
				<th width="150">DIA</th>
				<th>HORARIO</th>
			</tr>
			<tr>
				<td>Lunes</td>
				<td>
					<button class="button button-primary add_horario" type="button" style="float:right;">Añadir Horario</button>
					<div class="padre">
						<span>Desde:</span>
						<select name="horario_programado_desde[lunes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['horario_programado_desde']['lunes'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="horario_programado_hasta[lunes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['horario_programado_hasta']['lunes'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
					</div>

					<!-- ahora mostraremos a los hijos del selector-->
					<?php
						$i = 0;
					 foreach($mmyo_settings['horario_programado_desde']['lunes'] as $key_sem => $value_sem)
					 { 
					 	if($i==0){
					 		$i = 1;
					 		continue;
					 	}
					 ?>

					<div class="hijo">
						<span>Desde:</span>
						<select name="horario_programado_desde[lunes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['horario_programado_desde']['lunes'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="horario_programado_hasta[lunes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['horario_programado_hasta']['lunes'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<?php } ?>

				</td>
			</tr>
			<tr>
				<td>Martes</td>
				<td>
					<button class="button button-primary add_horario" type="button" style="float:right;">Añadir Horario</button>
					<div class="padre">
						<span>Desde:</span>
						<select name="horario_programado_desde[martes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['horario_programado_desde']['martes'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="horario_programado_hasta[martes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['horario_programado_hasta']['martes'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<!-- ahora mostraremos a los hijos del selector-->
					<?php
						$i = 0;
					 foreach($mmyo_settings['horario_programado_desde']['martes'] as $key_sem => $value_sem)
					 { 
					 	if($i==0){
					 		$i = 1;
					 		continue;
					 	}
					 ?>

					<div class="hijo">
						<span>Desde:</span>
						<select name="horario_programado_desde[martes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['horario_programado_desde']['martes'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="horario_programado_hasta[martes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['horario_programado_hasta']['martes'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<?php } ?>

				</td>
			</tr>
			<tr>
				<td>Miercoles</td>
				<td>
					<button class="button button-primary add_horario" type="button" style="float:right;">Añadir Horario</button>
					<div class="padre">
						<span>Desde:</span>
						<select name="horario_programado_desde[miercoles][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['horario_programado_desde']['miercoles'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="horario_programado_hasta[miercoles][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['horario_programado_hasta']['miercoles'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<!-- ahora mostraremos a los hijos del selector-->
					<?php
						$i = 0;
					 foreach($mmyo_settings['horario_programado_desde']['miercoles'] as $key_sem => $value_sem)
					 { 
					 	if($i==0){
					 		$i = 1;
					 		continue;
					 	}
					 ?>

					<div class="hijo">
						<span>Desde:</span>
						<select name="horario_programado_desde[miercoles][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['horario_programado_desde']['miercoles'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="horario_programado_hasta[miercoles][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['horario_programado_hasta']['miercoles'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<?php } ?>

				</td>
			</tr>
			<tr>
				<td>Jueves</td>
				<td>
					<button class="button button-primary add_horario" type="button" style="float:right;">Añadir Horario</button>
					<div class="padre">
						<span>Desde:</span>
						<select name="horario_programado_desde[jueves][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['horario_programado_desde']['jueves'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="horario_programado_hasta[jueves][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['horario_programado_hasta']['jueves'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<!-- ahora mostraremos a los hijos del selector-->
					<?php
						$i = 0;
					 foreach($mmyo_settings['horario_programado_desde']['jueves'] as $key_sem => $value_sem)
					 { 
					 	if($i==0){
					 		$i = 1;
					 		continue;
					 	}
					 ?>

					<div class="hijo">
						<span>Desde:</span>
						<select name="horario_programado_desde[jueves][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['horario_programado_desde']['jueves'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="horario_programado_hasta[jueves][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['horario_programado_hasta']['jueves'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<?php } ?>

				</td>
			</tr>
			<tr>
				<td>Viernes</td>
				<td>
					<button class="button button-primary add_horario" type="button" style="float:right;">Añadir Horario</button>
					<div class="padre">
						<span>Desde:</span>
						<select name="horario_programado_desde[viernes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['horario_programado_desde']['viernes'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="horario_programado_hasta[viernes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['horario_programado_hasta']['viernes'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<!-- ahora mostraremos a los hijos del selector-->
					<?php
						$i = 0;
					 foreach($mmyo_settings['horario_programado_desde']['viernes'] as $key_sem => $value_sem)
					 { 
					 	if($i==0){
					 		$i = 1;
					 		continue;
					 	}
					 ?>

					<div class="hijo">
						<span>Desde:</span>
						<select name="horario_programado_desde[viernes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['horario_programado_desde']['viernes'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="horario_programado_hasta[viernes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['horario_programado_hasta']['viernes'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<?php } ?>

				</td>
			</tr>
			<tr>
				<td>Sabado</td>
				<td>
					<button class="button button-primary add_horario" type="button" style="float:right;">Añadir Horario</button>
					<div class="padre">
						<span>Desde:</span>
						<select name="horario_programado_desde[sabado][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['horario_programado_desde']['sabado'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="horario_programado_hasta[sabado][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['horario_programado_hasta']['sabado'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<!-- ahora mostraremos a los hijos del selector-->
					<?php
						$i = 0;
					 foreach($mmyo_settings['horario_programado_desde']['sabado'] as $key_sem => $value_sem)
					 { 
					 	if($i==0){
					 		$i = 1;
					 		continue;
					 	}
					 ?>

					<div class="hijo">
						<span>Desde:</span>
						<select name="horario_programado_desde[sabado][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['horario_programado_desde']['sabado'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="horario_programado_hasta[sabado][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['horario_programado_hasta']['sabado'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<?php } ?>

				</td>
			</tr>
			<tr>
				<td>Domingo</td>
				<td>
					<button class="button button-primary add_horario" type="button" style="float:right;">Añadir Horario</button>
					<div class="padre">
						<span>Desde:</span>
						<select name="horario_programado_desde[domingo][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['horario_programado_desde']['domingo'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="horario_programado_hasta[domingo][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['horario_programado_hasta']['domingo'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<!-- ahora mostraremos a los hijos del selector-->
					<?php
						$i = 0;
					 foreach($mmyo_settings['horario_programado_desde']['domingo'] as $key_sem => $value_sem)
					 { 
					 	if($i==0){
					 		$i = 1;
					 		continue;
					 	}
					 ?>

					<div class="hijo">
						<span>Desde:</span>
						<select name="horario_programado_desde[domingo][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['horario_programado_desde']['domingo'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="horario_programado_hasta[domingo][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['horario_programado_hasta']['domingo'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<?php } ?>

				</td>
			</tr>
		</table>


		<!-- ESTE ES HORARIO RE RECOGO EN ALMACEN-->
		<hr>


			<h3>Recogo en almacen</h3>
			<h5>Mensaje</h5>
			<p>Añade el mensaje a mostrar en el metodo de envio Recojo Almacen</p>
			<input type="text" name="mmyo_mensaje_recojo_almacen" style="width: 90%; padding: 10px;" value="<?php echo @$mmyo_settings['mmyo_mensaje_recojo_almacen']; ?>">

			<hr>
			<h5>Distritos de LIMA en donde es operado</h5>
			<?php 
				$region_id = '150000';
				$provincias = wooshc_get_provinces_regions($region_id);


				$provincias_recojo_almacen = $mmyo_settings['provincias_recojo_almacen'];
				$distrito_recojo_almacen = $mmyo_settings['distrito_recojo_almacen'];
			?>
			<!-- En esta parte vamos a jalar todas las provincias que perternecen a la region de lima-->

			<div id="sistema-recojo-almacen">
				<button type="button" class="button button-primary" id="anadir-provincia">Añadir Provincia</button>
				<br> 
				<br>

				<div id="sistema-almacen" style="display: none;">
					<select class="provincia" name="provincias_recojo_almacen[]">
							<option value="">Seleccione la provincia</option>
									<?php 
									foreach ($provincias as $data) {
									?>
									<option value="<?php echo $data->id; ?>"><?php echo $data->name; ?></option>
							 <?php  } ?>
					</select>
					<select  name="distrito_recojo_almacen[]" class="js-example-basic-multiple distrito" multiple="multiple" style="width:50%; max-height: 100px;" placeholder="Seleccionar los distritos">
					</select>

					<button type="button" class="remove-data button-primary button">X</button>
				</div>

				<!-- VAMOS A COLOCAR EL FOREACH AQUI-->
				<?php 

					foreach($provincias_recojo_almacen as $keyra => $valuera){
				?>

					<div class="custom-select-recojo-almacen-distrito">
					<select class="provincia" name="provincias_recojo_almacen[]">
							<option value="">Seleccione la provincia</option>
									<?php 
									foreach ($provincias as $data) {
									?>
									<option <?php selected($provincias_recojo_almacen[$keyra],$data->id); ?> value="<?php echo $data->id; ?>"><?php echo $data->name; ?></option>
							 <?php  } ?>
					</select>
					<select  name="distrito_recojo_almacen[<?php echo $provincias_recojo_almacen[$keyra]; ?>][]" class="js-example-basic-multiple distrito" multiple="multiple" style="width:50%; max-height: 100px;" placeholder="Seleccionar los distritos">
						<?php 
							$province_id = $provincias_recojo_almacen[$keyra];
							$listado_distritos = wooshc_get_districts_province($province_id,$region_id); 
							foreach ($listado_distritos as $keyld) 
							{
								$lista_distritos = $distrito_recojo_almacen[$provincias_recojo_almacen[$keyra]];
						?>
							<option  <?php if(in_array($keyld->id,$lista_distritos)){ ?> selected="selected" <?php } ?> value="<?php echo $keyld->id; ?>"><?php echo $keyld->name; ?></option>
					<?php 
						} 
					?>
					</select>

					<button type="button" class="remove-data button-primary button">X</button>
				</div>
					
				<?php 
					}//cierre del foreach
				 ?>

			</div>

			<hr>

		
		<table width="70%" border="1" cellpadding="0" cellspacing="0">


			<h5>Horarios</h5>
			<tr>
				<th width="150">DIA</th>
				<th>HORARIO</th>
			</tr>
			<tr>
				<td>Lunes</td>
				<td>
					<button class="button button-primary add_horario" type="button" style="float:right;">Añadir Horario</button>
					<div class="padre">
						<span>Desde:</span>
						<select name="recojo_almacen_desde[lunes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['recojo_almacen_desde']['lunes'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="recojo_almacen_hasta[lunes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['recojo_almacen_hasta']['lunes'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
					</div>

					<!-- ahora mostraremos a los hijos del selector-->
					<?php
						$i = 0;
					 foreach($mmyo_settings['recojo_almacen_desde']['lunes'] as $key_sem => $value_sem)
					 { 
					 	if($i==0){
					 		$i = 1;
					 		continue;
					 	}
					 ?>

					<div class="hijo">
						<span>Desde:</span>
						<select name="recojo_almacen_desde[lunes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['recojo_almacen_desde']['lunes'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="recojo_almacen_hasta[lunes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['recojo_almacen_hasta']['lunes'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<?php } ?>

				</td>
			</tr>
			<tr>
				<td>Martes</td>
				<td>
					<button class="button button-primary add_horario" type="button" style="float:right;">Añadir Horario</button>
					<div class="padre">
						<span>Desde:</span>
						<select name="recojo_almacen_desde[martes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['recojo_almacen_desde']['martes'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="recojo_almacen_hasta[martes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['recojo_almacen_hasta']['martes'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<!-- ahora mostraremos a los hijos del selector-->
					<?php
						$i = 0;
					 foreach($mmyo_settings['recojo_almacen_desde']['martes'] as $key_sem => $value_sem)
					 { 
					 	if($i==0){
					 		$i = 1;
					 		continue;
					 	}
					 ?>

					<div class="hijo">
						<span>Desde:</span>
						<select name="recojo_almacen_desde[martes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['recojo_almacen_desde']['martes'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="recojo_almacen_hasta[martes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['recojo_almacen_hasta']['martes'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<?php } ?>

				</td>
			</tr>
			<tr>
				<td>Miercoles</td>
				<td>
					<button class="button button-primary add_horario" type="button" style="float:right;">Añadir Horario</button>
					<div class="padre">
						<span>Desde:</span>
						<select name="recojo_almacen_desde[miercoles][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['recojo_almacen_desde']['miercoles'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="recojo_almacen_hasta[miercoles][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['recojo_almacen_hasta']['miercoles'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<!-- ahora mostraremos a los hijos del selector-->
					<?php
						$i = 0;
					 foreach($mmyo_settings['recojo_almacen_desde']['miercoles'] as $key_sem => $value_sem)
					 { 
					 	if($i==0){
					 		$i = 1;
					 		continue;
					 	}
					 ?>

					<div class="hijo">
						<span>Desde:</span>
						<select name="recojo_almacen_desde[miercoles][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['recojo_almacen_desde']['miercoles'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="recojo_almacen_hasta[miercoles][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['recojo_almacen_hasta']['miercoles'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<?php } ?>

				</td>
			</tr>
			<tr>
				<td>Jueves</td>
				<td>
					<button class="button button-primary add_horario" type="button" style="float:right;">Añadir Horario</button>
					<div class="padre">
						<span>Desde:</span>
						<select name="recojo_almacen_desde[jueves][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['recojo_almacen_desde']['jueves'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="recojo_almacen_hasta[jueves][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['recojo_almacen_hasta']['jueves'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<!-- ahora mostraremos a los hijos del selector-->
					<?php
						$i = 0;
					 foreach($mmyo_settings['recojo_almacen_desde']['jueves'] as $key_sem => $value_sem)
					 { 
					 	if($i==0){
					 		$i = 1;
					 		continue;
					 	}
					 ?>

					<div class="hijo">
						<span>Desde:</span>
						<select name="recojo_almacen_desde[jueves][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['recojo_almacen_desde']['jueves'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="recojo_almacen_hasta[jueves][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['recojo_almacen_hasta']['jueves'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<?php } ?>

				</td>
			</tr>
			<tr>
				<td>Viernes</td>
				<td>
					<button class="button button-primary add_horario" type="button" style="float:right;">Añadir Horario</button>
					<div class="padre">
						<span>Desde:</span>
						<select name="recojo_almacen_desde[viernes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['recojo_almacen_desde']['viernes'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="recojo_almacen_hasta[viernes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['recojo_almacen_hasta']['viernes'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<!-- ahora mostraremos a los hijos del selector-->
					<?php
						$i = 0;
					 foreach($mmyo_settings['recojo_almacen_desde']['viernes'] as $key_sem => $value_sem)
					 { 
					 	if($i==0){
					 		$i = 1;
					 		continue;
					 	}
					 ?>

					<div class="hijo">
						<span>Desde:</span>
						<select name="recojo_almacen_desde[viernes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['recojo_almacen_desde']['viernes'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="recojo_almacen_hasta[viernes][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['recojo_almacen_hasta']['viernes'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<?php } ?>

				</td>
			</tr>
			<tr>
				<td>Sabado</td>
				<td>
					<button class="button button-primary add_horario" type="button" style="float:right;">Añadir Horario</button>
					<div class="padre">
						<span>Desde:</span>
						<select name="recojo_almacen_desde[sabado][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['recojo_almacen_desde']['sabado'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="recojo_almacen_hasta[sabado][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['recojo_almacen_hasta']['sabado'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<!-- ahora mostraremos a los hijos del selector-->
					<?php
						$i = 0;
					 foreach($mmyo_settings['recojo_almacen_desde']['sabado'] as $key_sem => $value_sem)
					 { 
					 	if($i==0){
					 		$i = 1;
					 		continue;
					 	}
					 ?>

					<div class="hijo">
						<span>Desde:</span>
						<select name="recojo_almacen_desde[sabado][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['recojo_almacen_desde']['sabado'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="recojo_almacen_hasta[sabado][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['recojo_almacen_hasta']['sabado'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<?php } ?>

				</td>
			</tr>
			<tr>
				<td>Domingo</td>
				<td>
					<button class="button button-primary add_horario" type="button" style="float:right;">Añadir Horario</button>
					<div class="padre">
						<span>Desde:</span>
						<select name="recojo_almacen_desde[domingo][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['recojo_almacen_desde']['domingo'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="recojo_almacen_hasta[domingo][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['recojo_almacen_hasta']['domingo'][0],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<!-- ahora mostraremos a los hijos del selector-->
					<?php
						$i = 0;
					 foreach($mmyo_settings['recojo_almacen_desde']['domingo'] as $key_sem => $value_sem)
					 { 
					 	if($i==0){
					 		$i = 1;
					 		continue;
					 	}
					 ?>

					<div class="hijo">
						<span>Desde:</span>
						<select name="recojo_almacen_desde[domingo][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option <?php selected($mmyo_settings['recojo_almacen_desde']['domingo'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<span>Hasta: </span>
						<select name="recojo_almacen_hasta[domingo][]">
							<?php foreach($array_horarios as $keyh => $valueh){ ?>
								<option  <?php selected($mmyo_settings['recojo_almacen_hasta']['domingo'][$key_sem],$array_horarios[$keyh]); ?> value="<?php echo $array_horarios[$keyh]; ?>"><?php echo $array_horarios[$keyh]; ?></option>
							<?php } ?>
						</select>
						<button type="button" class="button button-primary remove">X</button>
					</div>

					<?php } ?>

				</td>
			</tr>
		</table>


		<br>
		<br>

		<button type="submit" class="button button-primary">Guardar</button>
	</form>	

	<!-- vamos a crear el scrip para los horarios-->
	<script type="text/javascript">
		jQuery(document).ready(function($)
		{

			$(".add_horario").click(function()
			{
				new_horario = $(this).parent().find('div.padre').clone();
				new_horario = '<div class="hijo">'+new_horario.html()+'<button type="button" class="button button-primary remove">X</button></div>';
				$(this).parent().append(new_horario);
			});
			$(document).on('click','.remove',function()
			{
				$(this).parent().remove();
			});


			//vamos a seleccionar las provincias para mostrar los distritos
			jQuery(document).on('change','.provincia',function(){
						 select = jQuery(this);
						 idprovincia = $(this).val();
						// alert(jQuery(this).val());
						 var data = {
				            action: 'wooshc_get_district',
				            province_id: jQuery(this).val(),
				        };
				        jQuery.ajax({
				            type: 'POST',
				            url:'<?php echo admin_url("admin-ajax.php"); ?>',
				            data: data,
				            success: function(html){
				            	select.parent().find('select.distrito').html(html);
				            	select.parent().find('select.distrito').attr('name','distrito_recojo_almacen['+idprovincia+'][]');
				            }
				        });

			});
			$(".custom-select-recojo-almacen-distrito").find('.distrito').select2();


			//vamos a ir añadiendo los elementos 
			$(document).on("click",'#anadir-provincia',function(){
				html_sistema_almacen = $("#sistema-almacen").html();
				$("#sistema-recojo-almacen").append('<div class="custom-select-recojo-almacen-distrito">'+html_sistema_almacen+'</div>');
				//analizamos y colocamos el select2
				$(".custom-select-recojo-almacen-distrito").find('.distrito').select2();
			});
			$(document).on('click',".remove-data",function(){
				$(this).parent().remove();
			});

		});
	</script>

		
<?php 
}


add_action('admin_post_mmyo_settings_action','mmyo_settings_action_callback');
function mmyo_settings_action_callback()
{
	
	unset($_POST['distrito_recojo_almacen'][0]);
	unset($_POST['provincias_recojo_almacen'][0]);
	$mmyo_settings = $_POST;
	update_option('mmyo_settings',$mmyo_settings);

	wp_redirect(wp_get_referer());
}


//devolver array php en javascript
function js_array($array)
{	

    $temp = array_map('js_str', $array);
    return '[' . implode(',', $temp) . ']';
}
function js_str($s)
{
    return '"' . addcslashes($s, "\0..\37\"\\") . '"';
}