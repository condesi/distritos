<?php 
//add_action( 'woocommerce_review_order_before_payment', 'mmyo_horario_callback',99);
add_action( 'woocommerce_review_order_before_payment', 'mmyo_horario_callback');
function mmyo_horario_callback()
{
	$mmyo_settings = get_option('mmyo_settings',true);
	//var_dump($mmyo_settings['horario_programado_desde']['lunes']);
 	$array_semana = array('lunes','martes','miercoles','jueves','viernes','sabado','domingo');
 	$dias_disponibles = obtener_dia_semana_disponibles();
 	$dias_disponibles_recojo_almacen = obtener_dia_semana_disponibles_recojo_almacen();
 	$dia_hoy = get_dia_semana();


 	$array_dias_ordenados = array();
	$array_dias_no_contados = array();
	$encontro = false;
	if(count($dias_disponibles)>0)
	{
		foreach($dias_disponibles as $keydd => $valuedd)
		{	
			if($dias_disponibles[$keydd]!=$dia_hoy)
			{
				if($encontro == false)
				{
					array_push($array_dias_no_contados,$dias_disponibles[$keydd]);
				}else{
					array_push($array_dias_ordenados,$dias_disponibles[$keydd]);
				}
			}else{
				$encontro = true;
				array_push($array_dias_ordenados,$dias_disponibles[$keydd]);
			}
		}
	}
	if(count($array_dias_no_contados)>0)
	{
		foreach($array_dias_no_contados as $keydio => $valuedio)
		{
			array_push($array_dias_ordenados,$array_dias_no_contados[$keydio]);
		}
	}

?>


	<!-- vamos a mostrar los horarios disponibles-->
	<input type="hidden" class="horario_activo" name="horario_activo" value="">
	<div class="dias_semana" style="display: none;margin-bottom: 30px;background:#DE167E;padding:20px;">
		<h3 style="margin-bottom:15px;color:#fff;">Programa tu envío</h3>
		<h5  style="color:#fff;">Seleccione el día de entrega</h5>
		<select style="padding: 8px 16px;
    border: 1px solid transparent;
    border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
    cursor: pointer;
    user-select: none;margin-bottom: 15px;" name="mmyo_dia_semana" class="mmyo_dia_semana">
			<option  value="">Seleccione el dia de la semana</option>
			<?php 
				foreach($array_dias_ordenados as $key)
				{
					$dia_ingles = get_dia_ingles($key);
			?>
				<option style="margin:20px;" horario_desde='<?php echo js_array($mmyo_settings['horario_programado_desde'][$key]); ?>' value="<?php echo $key.' '.date('d/m/Y',strtotime("today $dia_ingles")); ?>"  horario_hasta='<?php echo js_array($mmyo_settings['horario_programado_hasta'][$key]); ?>'><?php echo $key.' '.date('d/m/Y',strtotime("today $dia_ingles")); ?></option>
			<?php 
				} 
			?>
		</select> <br> 
		<div class="hora_semana" style="display: none;">
			<h5  style="color:#fff;">Hora</h5>
			<select style="padding: 8px 16px;
    border: 1px solid transparent;
    border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
    cursor: pointer;
    user-select: none;" name="mmyo_hora_semana" class="mmyo_hora_semana">
			</select>
		</div>

	</div>


	
<?php 
		
	$array_dias_ordenados = array();
	$array_dias_no_contados = array();
	$encontro = false;
	if(count($dias_disponibles_recojo_almacen)>0)
	{
		foreach($dias_disponibles_recojo_almacen as $keydd => $valuedd)
		{	
			if($dias_disponibles_recojo_almacen[$keydd]!=$dia_hoy)
			{
				if($encontro == false)
				{
					array_push($array_dias_no_contados,$dias_disponibles_recojo_almacen[$keydd]);
				}else{
					array_push($array_dias_ordenados,$dias_disponibles_recojo_almacen[$keydd]);
				}
			}else{
				$encontro = true;
				array_push($array_dias_ordenados,$dias_disponibles_recojo_almacen[$keydd]);
			}
		}
	}
	if(count($array_dias_no_contados)>0)
	{
		foreach($array_dias_no_contados as $keydio => $valuedio)
		{
			array_push($array_dias_ordenados,$array_dias_no_contados[$keydio]);
		}
	}


?>



	<!-- ESTE OTRO ES PARA RECOJO EN ALMACEN-->
<?php /*	<input type="hidden" class="horario_activo_recojo_almacen" name="horario_activo_recojo_almacen" value="">
	<div class="dias_semana_almacen" style="display: none;margin-bottom: 30px;background:#DE167E;padding:20px;">
		<h3 style="margin-bottom:15px;color:#fff;">Programa Recogida en Almacén</h3>
		<h5  style="color:#fff;">Seleccione el día de entrega</h5>
		<select style="padding: 8px 16px;
    border: 1px solid transparent;
    border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
    cursor: pointer;
    user-select: none;margin-bottom: 15px;" name="mmyo_dia_semana_recojo_almacen" class="mmyo_dia_semana">
			<option  value="">Seleccione el dia de la semana</option>
			<?php 
				foreach($array_dias_ordenados as $key2)
				{
					$dia_ingles = get_dia_ingles($key2);
			?>
				<option style="margin:20px;" horario_desde='<?php echo js_array($mmyo_settings['recojo_almacen_desde'][$key2]); ?>' value="<?php echo $key2.' '.date('d/m/Y',strtotime("today $dia_ingles")); ?>"  horario_hasta='<?php echo js_array($mmyo_settings['recojo_almacen_hasta'][$key2]); ?>'><?php echo $key2.' '.date('d/m/Y',strtotime("today $dia_ingles")); ?></option>
			<?php 
				} 
			?>
		</select> <br> 
		<div class="hora_semana" style="display: none;">
			<h5  style="color:#fff;">Hora</h5>
			<select style="padding: 8px 16px;
    border: 1px solid transparent;
    border-color: transparent transparent rgba(0, 0, 0, 0.1) transparent;
    cursor: pointer;
    user-select: none;" name="mmyo_hora_semana_recojo_almacen" class="mmyo_hora_semana">
			</select>
		</div>

	</div>

*/
	?>

	<script type="text/javascript">
		jQuery(document).ready(function($)
		{
			$(".mmyo_dia_semana").change(function()
			{
				if($(this).val()!='')
				{
					$(".hora_semana").show(0);
					$(".mmyo_hora_semana").html('<option value="">Seleccione Hora</option>');
					horario_desde = $(this).find('option:selected').attr('horario_desde');
					horario_hasta= $(this).find('option:selected').attr('horario_hasta');
					horario_desde =  JSON.parse(horario_desde);
					horario_hasta = JSON.parse(horario_hasta);
					$.each(horario_desde, function (ind, elem) { 
					  //console.log('¡Hola :'+horario_desde[ind]+'!'); 
					  hora = horario_desde[ind]+'-'+horario_hasta[ind];
						$(".mmyo_hora_semana").append('<option value='+hora+'>'+hora+'</option>');
					});
				}else{
					$(".mmyo_hora_semana").html('');
					$(".hora_semana").hide(0);
				}
			});

			$(document).on('click','input[name="radio_choice"]',function()
			{
				//si estamos buscando la accion de programado mostramos el horario
				if($(this).val().indexOf('Programado') > (-1))
				{
					$(".horario_activo").attr('value','yes');
					$('.dias_semana').show(0);
				}else{

					$(".horario_activo").attr('value','');
					$('.dias_semana').hide(0);
				}

				/*if($(this).val().indexOf('almacen') > (-1))
				{
					$(".horario_activo_recojo_almacen").attr('value','yes');
					$('.dias_semana_almacen').show(0);
				}else{

					$(".horario_activo_recojo_almacen").attr('value','');
					$('.dias_semana_almacen').hide(0);
				}*/



			});



		});

	</script>
<?php
}

//vamos a obtener los dias de la semana disponible
function obtener_dia_semana_disponibles()
{
 	$array_semana = array('lunes','martes','miercoles','jueves','viernes','sabado','domingo');
	$mmyo_settings = get_option('mmyo_settings',true);
	$array_dias_semana = array();
	foreach($array_semana as $key)
	{
		if($mmyo_settings['horario_programado_desde'][$key][0] == '00:00AM' &&
		$mmyo_settings['horario_programado_hasta'][$key][0] == '00:00AM')
		{ continue; }
		array_push($array_dias_semana,$key);
	} 

	//ordenamos el arreglo de fechas
	
	return $array_dias_semana;
}


//VAMOS A OBTENER LOS DIAS DE LA SEMANA PERO de RECOJO E ALMACEN
function obtener_dia_semana_disponibles_recojo_almacen()
{
 	$array_semana = array('lunes','martes','miercoles','jueves','viernes','sabado','domingo');
	$mmyo_settings = get_option('mmyo_settings',true);
	$array_dias_semana = array();
	foreach($array_semana as $key)
	{
		if($mmyo_settings['recojo_almacen_desde'][$key][0] == '00:00AM' &&
		$mmyo_settings['recojo_almacen_hasta'][$key][0] == '00:00AM')
		{ continue; }
		array_push($array_dias_semana,$key);
	} 

	//ordenamos el arreglo de fechas
	
	return $array_dias_semana;
}


//vamos a sacar el dia de la semana
function get_dia_semana()
{
	$day = date("l");
	$diasemana = '';
	switch ($day) {
	    case "Sunday":
	           $diasemana = "domingo";
	    break;
	    case "Monday":
	           $diasemana = "lunes";
	    break;
	    case "Tuesday":
	           $diasemana = "martes";
	    break;
	    case "Wednesday":
	           $diasemana = "miercoles";
	    break;
	    case "Thursday":
	           $diasemana = "jueves";
	    break;
	    case "Friday":
	           $diasemana = "viernes";
	    break;
	    case "Saturday":
	           $diasemana = "sabado";
	    break;
	}
	return $diasemana;
}


//Sacar dia en ingles
function get_dia_ingles($day)
{
	$dia = '';
	switch ($day) {
	    case "domingo":
	           $dia = "sunday";
	    break;
	    case "lunes":
	           $dia =  "monday";
	    break;
	    case "martes":
	           $dia =  "tuesday";
	    break;
	    case "miercoles":
	           $dia =  "wednesday";
	    break;
	    case "jueves":
	           $dia =  "thursday";
	    break;
	    case "viernes":
	           $dia =  "friday";
	    break;
	    case "sabado":
	           $dia =  "saturday";
	    break;
	}
	return $dia;
}